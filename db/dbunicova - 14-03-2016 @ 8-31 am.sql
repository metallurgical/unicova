-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 14, 2016 at 01:30 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbunicova`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `a1` int(5) NOT NULL,
  `a2` int(5) NOT NULL,
  `a3` int(5) NOT NULL,
  `a4` int(5) NOT NULL,
  `a5` int(5) NOT NULL,
  `a6` int(5) NOT NULL,
  `a7` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `question_id`, `a1`, `a2`, `a3`, `a4`, `a5`, `a6`, `a7`) VALUES
(1, 1, 1, 2, 3, 4, 5, 6, 7),
(2, 2, 7, 6, 5, 4, 3, 2, 1),
(3, 3, 7, 6, 5, 4, 3, 2, 1),
(4, 4, 7, 6, 5, 4, 3, 2, 1),
(5, 5, 7, 6, 5, 4, 3, 2, 1),
(6, 6, 7, 6, 5, 4, 3, 2, 1),
(7, 7, 1, 2, 3, 4, 5, 6, 7),
(8, 8, 7, 6, 5, 4, 3, 2, 1),
(9, 9, 7, 6, 5, 4, 3, 2, 1),
(10, 10, 7, 6, 5, 4, 3, 2, 1),
(11, 11, 1, 2, 3, 4, 5, 6, 7),
(12, 12, 1, 2, 3, 4, 5, 6, 7),
(13, 13, 7, 6, 5, 4, 3, 2, 1),
(14, 14, 7, 6, 5, 4, 3, 2, 1),
(15, 15, 1, 2, 3, 4, 5, 6, 7),
(16, 16, 1, 2, 3, 4, 5, 6, 7),
(17, 17, 1, 2, 3, 4, 5, 6, 7),
(18, 18, 1, 2, 3, 4, 5, 6, 7),
(19, 19, 1, 2, 3, 4, 5, 6, 7),
(20, 20, 1, 2, 3, 4, 5, 6, 7),
(21, 21, 1, 2, 3, 4, 5, 6, 7),
(22, 22, 1, 2, 3, 4, 5, 6, 7),
(23, 23, 1, 2, 3, 4, 5, 6, 7),
(24, 24, 1, 2, 3, 4, 5, 6, 7),
(25, 25, 1, 2, 3, 4, 5, 6, 7),
(26, 26, 1, 2, 3, 4, 5, 6, 7),
(27, 27, 7, 6, 5, 4, 3, 2, 1),
(28, 28, 1, 2, 3, 4, 5, 6, 7),
(29, 29, 1, 2, 3, 4, 5, 6, 7),
(30, 30, 1, 2, 3, 4, 5, 6, 7),
(31, 31, 7, 6, 5, 4, 3, 2, 1),
(32, 32, 7, 6, 5, 4, 3, 2, 1),
(33, 33, 1, 2, 3, 4, 5, 6, 7),
(34, 34, 1, 2, 3, 4, 5, 6, 7),
(35, 35, 1, 2, 3, 4, 5, 6, 7);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `lang` varchar(30) NOT NULL,
  `category_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `lang`, `category_description`) VALUES
(1, 'bahagian1', 'english', 'STRONG BOND WITH THE CREATOR'),
(2, 'bahagian2', 'english', 'STEADFAST IN UPHOLDING SHARED PRINCIPLES'),
(3, 'bahagian3', 'english', 'CREATIVE IN MAKING WISE DECISIONS'),
(4, 'bahagian4', 'english', 'RESOLUTE IN FACING CHALLENGES'),
(5, 'bahagian5', 'english', 'PROACTIVE IN TAKING ACTIONS');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(11) NOT NULL,
  `faculty_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `faculty_name`) VALUES
(1, 'Audit Dalam'),
(2, 'Bahagian Keselamatan'),
(3, 'Fakulti Kejuruteraan Awam & Sumber Alam'),
(4, 'Fakulti Kejuruteraan Elektrik & Elektronik'),
(5, 'Fakulti Kejuruteraan Kimia & Sumber Asli'),
(6, 'Fakulti Kejuruteraan Mekanikal'),
(7, 'Fakulti Kejuruteraan Pembuatan'),
(8, 'Fakulti Pengurusan Industri'),
(9, 'Fakulti Sains & Teknologi Industri'),
(10, 'Fakulti Sistem Komputer & Kejuruteraan Perisian'),
(11, 'Fakulti Teknologi Kejuruteraan'),
(12, 'Institut Pengajian Siswazah'),
(13, 'Jabatan Bendahari'),
(14, 'Jabatan Hal Ehwal Akademik & Antarabangsa'),
(15, 'Jabatan Hal Ehwal Korporat & Kualiti'),
(16, 'Jabatan Hal Ehwal Pelajar & Alumni'),
(17, 'Jabatan Jaringan Industri & Masyarakat'),
(18, 'Jabatan Pembangunan & Pengurusan Harta'),
(19, 'Jabatan Pendaftar'),
(20, 'Jabatan Penyelidikan & Inovasi'),
(21, 'Makmal Berpusat Ump'),
(22, 'Pejabat Antarabangsa'),
(23, 'Pejabat Naib Canselor'),
(24, 'Penerbit Ump'),
(25, 'Perpustakaan'),
(26, 'Pusat Bahasa Moden & Sains Kemanusiaan'),
(27, 'Pusat Islam & Pembangunan Insan'),
(28, 'Pusat Kesihatan Pelajar'),
(29, 'Pusat Keusahawanan'),
(30, 'Pusat Pengajian Berterusan & Pembangunan Profesional\r\n'),
(31, 'Pusat Sukan'),
(32, 'Pusat Teknologi Maklumat & Komunikasi');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `lang` varchar(30) NOT NULL,
  `question_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `category_id`, `lang`, `question_name`) VALUES
(1, 1, 'english', 'If I am watching television and the call for prayer comes, I will perform my prayer.'),
(2, 1, 'english', 'I surf websites unrelated to my job during office hours'),
(3, 1, 'english', 'I am angry when my work is not appreciated'),
(4, 1, 'english', 'I feel that my job is the most important thing in my life'),
(5, 1, 'english', 'I use office stationery for my personal use too'),
(6, 1, 'english', 'I become hardworking  when my superiors observe my work\r\n'),
(7, 1, 'english', 'I am driven by religion during  my working hours'),
(8, 2, 'english', 'I help my friend to "check in" when asked to do so.'),
(9, 2, 'english', 'I am irritated when my work is criticised\r\n'),
(10, 2, 'english', 'When I need to raise ideas or opinions on certain issues, I choose to keep silent when my opinions and views differ from the others. '),
(11, 2, 'english', 'I do not make decision before deliberation.'),
(12, 2, 'english', 'Decision made in meetings is better than mine'),
(13, 2, 'english', 'After meetings, I voice my views to my colleagues outside meetings'),
(14, 2, 'english', 'When making monetary claims, I will claim a little more than the actual amount'),
(15, 3, 'english', 'I will think of a new approach to problem solving rather than using the approach that I am used to'),
(16, 3, 'english', 'My view on work-relation discussion often trigger more ideas from my colleagues.'),
(17, 3, 'english', 'I make effort to get information and assisstance from various resources as quick as possible to solve a difficult problem'),
(18, 3, 'english', 'In cases of complicated task/assignment I will delay completing it until the deadline\r\n'),
(19, 3, 'english', 'I feel guilty if I fail to handle a problem related to my job and responsibility reasonably'),
(20, 3, 'english', 'I am referred to by friends who need advice on work-related matters'),
(21, 3, 'english', 'If I have doubts that my decision will have negative effect to others, I choose not to make any decision'),
(22, 4, 'english', 'If I  feel that my job does not work well, I will seek other job'),
(23, 4, 'english', 'I am considered as a risk taker\r\n'),
(24, 4, 'english', 'I go home late to finish the jobs assigned to me.'),
(25, 4, 'english', 'If I need to do something important, I will work hard to complete it '),
(26, 4, 'english', 'I will avoid being selfish for the importance of my organisation'),
(27, 4, 'english', 'I am not strong to overcome too many challenges in doing my job'),
(28, 4, 'english', 'In carrying out a task I know how to utilize all the resources and identify the challenges.'),
(29, 5, 'english', 'I initiate actions to develop a better career for my future'),
(30, 5, 'english', 'I ensure that the outcome of my work  is the best that I could produce\r\n'),
(31, 5, 'english', 'My contribution to the organization is minimal'),
(32, 5, 'english', 'Most of my work-related problems remain unsolved'),
(33, 5, 'english', 'I think a lot of what I should do in future '),
(34, 5, 'english', 'I make effort to achieve more than the KPI statement'),
(35, 5, 'english', 'I make decision by examining the effects from all aspects');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `result_id` int(11) NOT NULL,
  `ic` varchar(50) NOT NULL,
  `faculty` text NOT NULL,
  `result_total_category_1` varchar(50) NOT NULL,
  `result_total_category_2` varchar(50) NOT NULL,
  `result_total_category_3` varchar(50) NOT NULL,
  `result_total_category_4` varchar(50) NOT NULL,
  `result_total_category_5` varchar(50) NOT NULL,
  `result_total_all` varchar(50) NOT NULL,
  `result_score_category_1` varchar(50) NOT NULL,
  `result_score_category_2` varchar(50) NOT NULL,
  `result_score_category_3` varchar(50) NOT NULL,
  `result_score_category_4` varchar(50) NOT NULL,
  `result_score_category_5` varchar(50) NOT NULL,
  `result_score_all` varchar(50) NOT NULL,
  `result_date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`result_id`, `ic`, `faculty`, `result_total_category_1`, `result_total_category_2`, `result_total_category_3`, `result_total_category_4`, `result_total_category_5`, `result_total_all`, `result_score_category_1`, `result_score_category_2`, `result_score_category_3`, `result_score_category_4`, `result_score_category_5`, `result_score_all`, `result_date_created`) VALUES
(40, '999999-99-9999', 'Jabatan Hal Ehwal Akademik & Antarabangsa', '22', '20', '42', '24', '29', '137', '44', '40', '85', '48', '59', '55.91836734693878', '2016-03-13 16:25:56'),
(42, '111111-11-1111', 'Fakulti Kejuruteraan Pembuatan', '23', '27', '28', '31', '34', '143', '46', '55', '57', '63', '69', '58.36734693877551', '2016-03-13 20:56:38');

-- --------------------------------------------------------

--
-- Table structure for table `surveyquestion`
--

CREATE TABLE `surveyquestion` (
  `id` int(50) NOT NULL,
  `question` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `a1` varchar(50) NOT NULL,
  `a2` varchar(50) NOT NULL,
  `a3` varchar(50) NOT NULL,
  `a4` varchar(50) NOT NULL,
  `a5` varchar(50) NOT NULL,
  `a6` varchar(50) NOT NULL,
  `a7` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surveyquestion`
--

INSERT INTO `surveyquestion` (`id`, `question`, `category`, `a1`, `a2`, `a3`, `a4`, `a5`, `a6`, `a7`) VALUES
(1, 'Sekiranya saya sedang menonton TV, dan masuk waktu solat, saya akan pergi menunaikan solat', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(2, 'Saya melayari laman web yang tidak berkaitan dengan tugas saya pada waktu kerja?', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(3, 'Saya berasa marah apabila hasil kerja saya tidak dihargai', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(4, 'Saya berasa bahawa pekerjaan adalah perkara paling utama dalam hidup saya', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(5, 'Saya menggunakan alatulis dan barangan pejabat untuk kegunaan peribadi', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(6, 'Saya menjadi rajin bekerja apabila ketua melihat apa yang saya buat', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(7, 'Agama memandu tindak tanduk saya sepanjang waktu saya bekerja', 'SB', '1', '2', '3', '4', '5', '6', '7'),
(8, 'Saya tolong ‘check-in’ bagi pihak kawan saya jika dia meminta saya berbuat demikian', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(9, 'Saya sakit hati apabila orang mengkritik kerja saya', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(10, 'Apabila saya perlu menyuarakan pandangan, dan pandangan saya berbeza dengan orang lain, saya berasa ragu dan memilih untuk berdiam diri', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(11, 'Saya tidak membuat keputusan sebelum berbincang', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(12, 'Keputusan mesyuarat lebih baik dari keputusan saya', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(13, 'Selepas mesyuarat, saya bercerita dan menyatakan pandangan peribadi saya kepada rakan-rakan di luar mesyuarat', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(14, 'Apabila membuat tuntutan bayaran, saya akan menuntut lebih sedikit dari yang sepatutnya', 'ST', '1', '2', '3', '4', '5', '6', '7'),
(15, 'Saya akan memikirkan penyelesaian baru berbanding mengguna cara lama untuk menyelesaikan masalah', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(16, 'Pandangan yang saya berikan dalam diskusi berkaitan kerja selalu menjadi pencetus kepada idea-idea daripada rakan-rakan yang lain', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(17, 'Saya akan berusaha mendapatkan maklumat dan bantuan daripada pelbagai sumber dengan kadar segera bagi mendapatkan penyelesaian kepada permasalahan yang sukar', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(18, 'Bagi tugasan kerja yang rumit, saya akan tangguhkan sehingga hampir kepada deadline baharulah saya cuba menyelesaikannya', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(19, 'Saya rasa bersalah sekiranya sesuatu permasalahan berkaitan tugas dan tanggungjawab saya masih belum dapat saya selesaikan dengan baik', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(20, 'Saya dihubungi oleh rakan-rakan yang menghadapi masalah dalam urusan kerja bagi mendapatkan pandangan berkaitan kerja tersebut', 'CT', '1', '2', '3', '4', '5', '6', '7'),
(21, 'Jika saya bimbang keputusan yang saya buat akan mendatangkan kesan negatif kepada orang lain, saya memilih untuk tidak membuat keputusan', 'CT', '1', '2', '3', '4', '5', '6', '7');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1457914975, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`result_id`);

--
-- Indexes for table `surveyquestion`
--
ALTER TABLE `surveyquestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `surveyquestion`
--
ALTER TABLE `surveyquestion`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

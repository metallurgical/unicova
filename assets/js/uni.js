$( document ).on( {

    keyup : function ( e ) {

        var 
            $this = $( this ),
            textVal = $this.val();

        if ( !checkingstring( textVal ) ) {

            alert( 'Number only' );
            $this.val( textVal.substring( 0, textVal.length - 1 ) );
            return;

        }

        if ( textVal.length == 7 ) {

            if ( textVal.indexOf( '-' ) === -1 ) {

                var splitedStr = textVal.split('');
                splitedStr.splice( 6, 0, '-' );
                $this.val( splitedStr.join('') );
            }
        }
        else if ( textVal.length == 10 ) {

            if ( textVal.indexOf( '-' ) !== -1 ) {

                if ( e.which !== 8 ) {
                    var splitedStr = textVal.split('');
                    splitedStr.splice( 9, 0, '-' );
                    $this.val( splitedStr.join('') );
                }
            }
        }
        
        if ( /[0-9]{6}/ig.test( textVal ) && ( textVal.length == 6 && textVal.length < 7 ) ) {

            if ( e.which !== 8 ) $this.val( textVal + '-' );

        }
        else if ( /[0-9]{6}\-[0-9]{2}/ig.test( textVal ) && textVal.length == 9 ) {

            if ( e.which !== 8 ) $this.val( textVal + '-' );

        }
        

        
    },
    paste : function ( e ) {

        setTimeout( function () {

            var 
                $this = $( e.target ),
                textVal = $this.val().toString(),
                length = textVal.length,
                splitedStr;

            if ( /[0-9]{6}\-[0-9]{2}\-[0-9]{4}/ig.test( textVal ) ) return;

            splitedStr = textVal.split('');

            if ( length >= 9 ) {

                splitedStr.splice( 6, 0, '-' );
                splitedStr.splice( 9, 0, '-' );
                $this.val( splitedStr.join('') );
            }

        }, 0 );


    },
    blur : function ( e ) {

        var 
            $this = $( e.target ),
            textVal = $this.val().toString(),
            length = textVal.length,
            splitedStr;

        if ( /[0-9]{6}\-[0-9]{2}\-[0-9]{4}/ig.test( textVal ) ) return;

        splitedStr = textVal.split('');

        if ( length >= 9 ) {

            splitedStr.splice( 6, 0, '-' );
            splitedStr.splice( 9, 0, '-' );
            $this.val( splitedStr.join('') );
        }

    }
}, '#ic, #searchInput' );

function checkingLenght ( input ) {

    return input.length;
}

function checkingstring ( input ) {

    var flag = false;

    if ( /^[\d\s\-\b]+$/ig.test( input ) === true ) {
        flag = true;
    }

    //console.log(flag)

    return flag;
}

function getConfig () {

    var conf = {

            chart: {                
                polar: true,
                type: 'area',
                renderTo : 'graffContainer'
            },

            title: {
                text: 'Result Uni-Cova',
                x: -80
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: ['CV1', 'CV2', 'CV3', 'CV4', 'CV5'],
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
            },

            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 70,
                layout: 'vertical'
            },

            series: [{
                name: 'Score',
                data: [],
                pointPlacement: 'on'
            }]

    }

    return conf;
}
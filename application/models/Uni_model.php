<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**

    +-+-+-+-+-+-+
    |M|y|U|P|I|S|
    +-+-+-+-+-+-+

 * My Enrollment / My Unit Pengambilan Institusi Pelajar [MyUPIS]
 *
 * https://www.myupis.com/
 * https://git.myupis.com/
 *
 * PHP version 5
 *
 * @category   models
 * @package    Us_model.php
 * @author     Nizam <nizam@myupis.com>
 * @author     Norlihazmey <norlihazmey@myupis.com>
 * @author     Afiq <afiq@myupis.com>
 * @license    http://www.codeigniter.com/userguide3/license.html
 * @copyright  2015 MyUPIS Sdn Bhd
 * @version    0.9.5
 *
*/

class Uni_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }   



    /**
     * [insert_new_data insert data into table - any type of table]
     * @param  [type] $arrayData [data value received from controller(column and value already)]
     * @param  [type] $table     [name of table to insert the data]
     * @return [type]            [description]
     */
    function insert_new_data($arrayData,$table)
    {

        $this->db->insert($table,$arrayData);
        //if ($this->db->_error_message()) {
             //return 'sdsd';
        //}
        if ( $this->db->affected_rows() > 0 ) {

            return $this->db->insert_id();

        } else {

            $flag = false;
            return $flag;
        }
       
        
    }

    /**
     * [delete_data delete all type of table]
     * @param  [type] $table [name of table]
     * @param  [type] $where [condition to applied]
     * @return [type]        [description]
     */
    public function delete_data($table, $where){

        $this->db->where($where);
        $this->db->delete($table);

        //return $this->db->affected_rows() > 0;
        
        if ( $this->db->affected_rows() > 0) {
            // delete && no error
            $a = 1; 
        } 
        else if ( $this->db->affected_rows() == 0 ) {
            // not delete && no error
            $a = 2;            
        }
        else {
            // not delete && have error
            $a = 3;
        }

        return $a;
    }

 

    function getLastData( $table, $uniqueField, $fields = false ) {

        if ( $fields ) {

             $column = $fields;

        }
        else {

            $column = '*' ;

        }

        $this->db->select( $column );
        $this->db->from( $table );
        $this->db->order_by( $uniqueField ,'desc');
        $this->db->limit( 1 );

        $query = $this->db->get();
        return $query->row_array();   
    }

    /**
     * [get_all_rows description]
     * @param  [type]  $table           [Compulsary : Table name that want to select]
     * @param  boolean $where           [Optional : where condition to select, if not state will select all data]
     * @param  boolean $tableNameToJoin [Optional : The name of table to join]
     * @param  boolean $tableRelation   [Optional : Relation of table to join]
     * @param  boolean $order_by        [Optional : Order by descending or ascending]
     * @return [type]                   [description]
     * Example usage
     *
     * Example 1 : Klu nk select data tanpa condition dan tanpa join
     * $table = "users";
     * $this->seat_model->get_all_rows($table);
     *
     * Example 2 : Klu nk select data dengan condition dan tanpa join
     * $table = "users";
     * $where = array('user_id' => $user_id);
     * $this->seat_model->get_all_rows($table, $where);
     * 
     * Example 3 : Klu nk select data tanpa condition dan dengan join(join satu table)
     * $table = "users";
     * $tableNameToJoin = array('branches');
     * $tableRelation = array('users.branch_id = branches.branch_id');
     * $this->seat_model->get_all_rows($table, $where, $tableNameToJoin , $tableRelation);
     *
     * Example 4 : Klu nk select data tanpa condition dan dengan join(join 2 table)
     * $table = "users";
     * $tableNameToJoin = array('course','semester');
     * $tableRelation = array('users.course_id = course.course_id',
     *                        'users.semester_id = semester.course_id'
     *                       );
     * $this->seat_model->get_all_rows($table, $where, $tableNameToJoin , $tableRelation);
     * Begitulah seterusnya untuk join table lain, tambah ikut suka
     *
     * Example 5 : Klu nk select data dengan condition dan dengan join(join satu table)
     * $table = "users";
     * $tableNameToJoin = array('branches');
     * $tableRelation = array('users.branch_id = branches.branch_id');
     * $where = array('users.user_id' => $user_id);
     * $this->seat_model->get_all_rows($table, $where, $tableNameToJoin , $tableRelation);
     */
    function get_all_rows($table, $where = false, $fields = false, $tableNameToJoin = false, $tableRelation = false, $order_by = false, $group_by = false)
    {
           // $this->db->distinct();
            if($fields){
                $this->db->select($fields);
            }else{
                 $this->db->select('*');
            }
            
            $this->db->from($table);

            if($where!=false){
               $this->db->where($where);
            }
           
           if($order_by!=false){
                $this->db->order_by($order_by[0], $order_by[1]);
           }
           
            if($group_by!=false){
                // for ($i=0; $i < count($group_by); $i++){
                    $this->db->group_by($group_by);
                //}
            
            }
            
           if($tableNameToJoin!=false && $tableRelation!=false){
                for ($i=0; $i < count($tableNameToJoin); $i++){
                    $this->db->join($tableNameToJoin[$i], $tableRelation[$i]);
                }
                
           }
          
            $query = $this->db->get();
            return $query->result_array(); 
           
    }

    // source
    // http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
    function isAssoc ( $arr ) {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    function get_all_traditional($table, $where = false, $fields = false, $tableRelation = false, $order_by = false, $group_by = false, $limit = false)
    {
           // $this->db->distinct();
            if($fields) {
                $this->db->select($fields, false);
            }else{
                 $this->db->select('*');
            }
            
            $this->db->from($table);

            if($where!=false){
               $this->db->where($where);
            }
           
            if($order_by!=false) {

                if ( $this->isAssoc( $order_by ) ) {

                    foreach ( $order_by as $key => $value ) {
                        $this->db->order_by( $key, $value );
                    }
                }
                else {
                    $this->db->order_by($order_by[0], $order_by[1]);
                }     

           }
           
            if($group_by!=false){
                // for ($i=0; $i < count($group_by); $i++){
                    $this->db->group_by($group_by);
                //}
            
            }

            if($limit!=false){
                
                $this->db->limit($limit);

            }
               
          
            $query = $this->db->get();
            return $query->result_array(); 
            
           
    }
    

    /**
     * [get_all_rows description]
     * @param  [type]  $table           [Compulsary : Table name that want to select]
     * @param  boolean $where           [Optional : where condition to select, if not state will select all data]
     * @param  boolean $tableNameToJoin [Optional : The name of table to join]
     * @param  boolean $tableRelation   [Optional : Relation of table to join]
     * @param  boolean $order_by        [Optional : Order by descending or ascending]
     * @return [type]                   [description]
     * Example usage
     *
     * Example 1 : Klu nk select data tanpa condition dan tanpa join
     * $table = "users";
     * $this->seat_model->get_specified_row($table);
     *
     * Example 2 : Klu nk select data dengan condition dan tanpa join
     * $table = "users";
     * $where = array('user_id' => $user_id);
     * $this->seat_model->get_specified_row($table, $where);
     * 
     * Example 3 : Klu nk select data tanpa condition dan dengan join(join satu table)
     * $table = "users";
     * $tableNameToJoin = array('branches');
     * $tableRelation = array('users.branch_id = branches.branch_id');
     * $this->seat_model->get_specified_row($table, $where, $tableNameToJoin , $tableRelation);
     *
     * Example 4 : Klu nk select data tanpa condition dan dengan join(join 2 table)
     * $table = "users";
     * $tableNameToJoin = array('course','semester');
     * $tableRelation = array('users.course_id = course.course_id',
     *                        'users.semester_id = semester.course_id'
     *                       );
     * $this->seat_model->get_specified_row($table, $where, $tableNameToJoin , $tableRelation);
     * Begitulah seterusnya untuk join table lain, tambah ikut suka
     *
     * Example 5 : Klu nk select data dengan condition dan dengan join(join satu table)
     * $table = "users";
     * $tableNameToJoin = array('branches');
     * $tableRelation = array('users.branch_id = branches.branch_id');
     * $where = array('users.user_id' => $user_id);
     * $this->seat_model->get_specified_row($table, $where, $tableNameToJoin , $tableRelation);
     */
    function get_specified_row($table, $where = false, $fields = false, $tableNameToJoin = false, $tableRelation = false, $order_by = false)
    {
        
        if($fields){
            $this->db->select($fields);
        }else{
             $this->db->select('*');
        }
        $this->db->from($table);

        if($where!=false)
        {
             $this->db->where($where); 
        }

        if($order_by!=false)
        {
            $this->db->order_by($order_by[0], $order_by[1]);
        }

        if($tableNameToJoin!=false && $tableRelation!=false){
                for ($i=0; $i < count($tableNameToJoin); $i++){
                    $this->db->join($tableNameToJoin[$i], $tableRelation[$i]);
                }
                
        }

        $query = $this->db->get();
        return $query->row_array();    
    }
   

    /**
     * [update_data update datas in any tables you want]
     * @param  [array] $columnToUpdate [what column you want to update = array value]
     * @param  [string] $tableToUpdate  [what table you want to update]
     * @param  [array] $usingCondition [using condition or not]
     * @return [type]                 [description]
     */
    
    function update_data($columnToUpdate, $tableToUpdate, $usingCondition)
    {
        
        $this->db->where($usingCondition);
        $this->db->update($tableToUpdate, $columnToUpdate);

        if ( $this->db->affected_rows() > 0) {
            // update && no error
            $a = 1; 
        } 
        else if ( $this->db->affected_rows() == 0 ) {
            // not update && no error
            $a = 2;            
        }
        else {
            // not update && have error
            $a = 3;
        }

        return $a;



    }

    /**
     * [display_message display flash message data in view part]
     * @param  [string] $messageType [what type of message you want to display]
     * @param  [string] $urlToGo     [url you want to redirect after making the process ==> if using current url just use current_url()]
     * @return [type]              [description]
     * currently only 3 types of message can appear on page (save, record, error) ->can change the if else to add another type of message
     */
    function display_message( $messageType, $urlToGo =  false )
    {
        /**
         * usage :
         * calling this method in controller of course
         * example : display_message("save","jobs/index/add")
         * 1st parameter $messageType is compulsary : accept value insert,update,delete,error,email
         * 2nd paramter $urlToGo is optional : default direction will be given if not specified
         * @var [type]
         */
        if( $messageType == "add" ) {
             $this->session->set_flashdata( 'add', lang( 'common_alert_success' ) );
        }
        else if( $messageType == "update" ) {
             $this->session->set_flashdata( 'update', lang( 'common_alert_update' ) );
        }
        else if( $messageType == "error" ) {
             $this->session->set_flashdata( 'error', lang( 'common_alert_error' ) );
        }
        else if( $messageType == "delete" ) {
             $this->session->set_flashdata( 'delete', lang( 'common_alert_delete' ) );
        }
        else if( $messageType == "login_false" ) {
             $this->session->set_flashdata( 'login_false', 'Incorrect Username or Password' );
        }
        
              
       

        if($urlToGo == false) {
             $url = current_url();
        }
        else {
             $url = $urlToGo;
        }
            
        return redirect($url);
    }

}

/* End of file Us_model.php */
/* Location: ./application/models/Us_model.php */
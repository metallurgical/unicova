<?php

// header
$lang['header_sitename'] = "Uni-Cova";

// footer
$lang['footer_admin'] = 'Pentadbir';

// jumbotron
$lang['jumbo_heading'] = "Nilai Teras Universiti";
$lang['jumbo_desc']    = "Soal selidik ini adalah berkaitan dengan diri anda sebagai staf UMP dan berkaitan dengan ruang lingkup pekerjaan anda semata-mata, tandakan mengikut skala berikut :";

// menus
$lang['menu_check_result'] = 'Check Result';

// menus admin
$lang['menu_admin_results']     = 'Keputusan';
$lang['menu_admin_view_result'] = 'Carian';
$lang['menu_admin_list_data']   = 'Senarai Data';
$lang['menu_admin_by_faculty']  = 'Mengikut Fakulti';
$lang['menu_admin_menus']       = 'Menus';
$lang['menu_admin_']            = '';

// sitelogo admin
$lang['sitelogo_welcome'] = 'Selamat Datang';
$lang['sitelogo_admin'] = 'Pentadbir';


// common btn
$lang['common_btn_signin']          = 'Log Masuk';
$lang['common_btn_logout']          = 'Log Keluar';
$lang['common_btn_search']          = 'Cari';
$lang['common_btn_agree']           = 'Sangat Setuju';
$lang['common_btn_1']               = '1';
$lang['common_btn_2']               = '2';
$lang['common_btn_3']               = '3';
$lang['common_btn_4']               = '4';
$lang['common_btn_5']               = '5';
$lang['common_btn_6']               = '6';
$lang['common_btn_7']               = '7';
$lang['common_btn_disagree']        = 'Sangat Tidak Setuju';
$lang['common_btn_click_to_start']  = 'Mulakan Uni-Cova';
$lang['common_btn_back']            = 'Kembali';
$lang['common_btn_search']          = 'Cari';
$lang['common_btn_home']            = 'Utama';
$lang['common_ic']                  = 'No IC';
$lang['common_dalil']               = 'Dalil';
$lang['common_category']            = 'Kategori';
$lang['common_alert_no_data_found'] = 'Rekod tidak dijumpai';


// questions
$lang['cat_section_title'] = 'Bahagian';

// panduan
$lang['instruct_core_value_title']          = ' NILAI TERAS';
$lang['instruct_core_value_content']        = ' Nilai teras adalah sesuatu nilai yang menjadi panduan oleh organisasi dan seluruh warga kerjanya. Jika nilai-nilai yang dipegang itu adalah baik dan murni, maka nilai yang dibawa oleh organisasi tersebut adalah nilai yang positif. Sebaliknya, jika nilai itu tidak baik, maka nilai tersebut menjadi nilai yang negatif.

Nilai-nilai positif ini sangat berkaitrapat dengan budaya kerja korporat sesebuah organisasi itu. Jika nilai-nilai yang diterapkan dan dihayati itu murni dan positif, secara tidak langsung ia akan membawa budaya kerja dan produktiviti yang cemerlang. Aspirasi dan penghayatan kepada unsur-unsur nilai teras serta ciri-ciri budaya kerja korporat oleh setiap warga kerja Universiti Malaysia Pahang (UMP) mampu meningkatkan kualiti dan menjana modal insan yang berpengetahuan dan kreatif seterusnya dapat merealisasikan hasrat dan visi penubuhan UMP.

Setiap warga kerja UMP perlu berakhlak dan beretika mulia, mengamalkan nilai-nilai murni, memiliki nilai-nilai keagamaan dan kerohanian yang utuh dan kukuh yang berteraskan budaya dan imej korporat universiti. Universiti Malaysia Pahang telah mengenalpasti dan menetapkan lima Nilai Teras yang bertujuan memberi input dan tambah nilai kepada setiap warga kerja UMP untuk meningkatkan kecemerlangan diri selaras dengan falsafah kegemilangan universiti iaitu:';
$lang['instruct_core_value_content_list_1']          = 'Hubungan yang kuat dengan Yang Maha Pencipta';
$lang['instruct_core_value_content_list_2']          = 'Teguh dalam mempertahankan prinsip yang disepakati';
$lang['instruct_core_value_content_list_3']          = 'Kreatif dan bijaksana dalam membuat keputusan';
$lang['instruct_core_value_content_list_4']          = 'Cekal dalam menghadapi cabaran';
$lang['instruct_core_value_content_list_5']          = 'Proaktif dalam tindakan';

$lang['instruct_core_value_section_main_title_']     = 'NILAI TERAS: PERKARA-PERKARA YANG PATUT DIAMALKAN DAN PATUT DIJAUHI';
// 1
$lang['instruct_core_value_section_1_title']         = 'Hubungan yang kuat dengan Yang Maha Pencipta';
$lang['instruct_core_value_section_1_sub_title_1']   = 'Jadikan amalan';
$lang['instruct_core_value_section_1_sub_content_11'] = 'Mengutamakan suruhan Allah';
$lang['instruct_core_value_section_1_sub_content_12'] = 'Menjadikan kerja sebagai ibadah';
$lang['instruct_core_value_section_1_sub_content_13'] = 'Bekerja untuk mendapatkan keredhaan Allah';
$lang['instruct_core_value_section_1_sub_content_14'] = 'Tidak menjadikan pekerjaan sebagai tujuan utama hidup';
$lang['instruct_core_value_section_1_sub_content_15'] = 'Menggunakan barangan pejabat untuk tujuan kerja sahaja';
$lang['instruct_core_value_section_1_sub_content_16'] = 'Bekerja bukan untuk menunjuk-nunjuk';
$lang['instruct_core_value_section_1_sub_content_17'] = 'Agama memandu tindak tanduk ketika bekerja';
$lang['instruct_core_value_section_1_sub_title_2']   = 'Berusaha jauhi';
$lang['instruct_core_value_section_1_sub_content_21'] = 'Melalaikan suruhan dan larangan Allah';
$lang['instruct_core_value_section_1_sub_content_22'] = 'Banyak melakukan perkara yang tidak berkaitan kerja pada masa kerja';
$lang['instruct_core_value_section_1_sub_content_23'] = 'Marah apabila tidak dihargai manusia';
$lang['instruct_core_value_section_1_sub_content_24'] = 'Mengutamakan kerja lebih dari segala-galanya';
$lang['instruct_core_value_section_1_sub_content_25'] = 'Menggunakan barangan pejabat untuk kegunaan peribadi';
$lang['instruct_core_value_section_1_sub_content_26'] = 'Menjadi rajin apabila dilihat orang';
$lang['instruct_core_value_section_1_sub_content_27'] = 'Menganggap pekerjaan tiada hubungan dengan agama';
// 2
$lang['instruct_core_value_section_2_title']         = 'Teguh dalam mempertahankan prinsip yang disepakati';
$lang['instruct_core_value_section_2_sub_title_1']   = 'Jadikan amalan';
$lang['instruct_core_value_section_2_sub_content_11'] = 'Tidak membantu kawan dalam perkara yang salah';
$lang['instruct_core_value_section_2_sub_content_12'] = 'Menerima kritikan dengan terbuka';
$lang['instruct_core_value_section_2_sub_content_13'] = 'Berani memberi pandangan walaupun sangat berbeza dengan pandangan orang lain';
$lang['instruct_core_value_section_2_sub_content_14'] = 'Selalu berbincang sebelum membuat keputusan';
$lang['instruct_core_value_section_2_sub_content_15'] = 'Akur bahawa keputusan mesyuarat adalah lebih baik dari keputusan sendiri';
$lang['instruct_core_value_section_2_sub_content_16'] = 'Menahan diri dari mengkritik keputusan mesyuarat di luar mesyuarat';
$lang['instruct_core_value_section_2_sub_content_17'] = 'Membuat tuntutan bayaran mengikut yang selayaknya';
$lang['instruct_core_value_section_2_sub_title_2']   = 'Berusaha jauhi';
$lang['instruct_core_value_section_2_sub_content_21'] = 'Tolong menolong dalam perkara yang salah';
$lang['instruct_core_value_section_2_sub_content_22'] = 'Sakit hati apabila dikritik';
$lang['instruct_core_value_section_2_sub_content_23'] = 'Suka berdiam diri dan berasa ragu-ragu untuk mengeluarkan pendapat';
$lang['instruct_core_value_section_2_sub_content_24'] = 'Membuat keputusan sebelum berbincang';
$lang['instruct_core_value_section_2_sub_content_25'] = 'Menganggap keputusan sendiri lebih baik dari keputusan mesyuarat';
$lang['instruct_core_value_section_2_sub_content_26'] = 'Bercerita dan menyatakan pandangan peribadi kepada rakan-rakan di luar mesyuarat';
$lang['instruct_core_value_section_2_sub_content_27'] = 'Membuat tuntutan bayaran lebih dari yang sepatutnya';
// 3
$lang['instruct_core_value_section_3_title']         = 'Kreatif dalam membuat keputusan yang bijaksana';
$lang['instruct_core_value_section_3_sub_title_1']   = 'Jadikan amalan';
$lang['instruct_core_value_section_3_sub_content_11'] = 'Cuba mencari cara baru menyelesaikan masalah dengan lebih efektif';
$lang['instruct_core_value_section_3_sub_content_12'] = 'Menjadi pencetus idea-idea baru';
$lang['instruct_core_value_section_3_sub_content_13'] = 'Berusaha mendapatkan sebanyak-banyak maklumat sebelum menyelesai masalah yang sukar';
$lang['instruct_core_value_section_3_sub_content_14'] = 'Tidak menangguh tugasan sehingga ke \'deadline\'';
$lang['instruct_core_value_section_3_sub_content_15'] = 'Sentiasa berusaha selagi masalah belum selesai dengan baik';
$lang['instruct_core_value_section_3_sub_content_16'] = 'Sentiasa membantu rakan-rakan menyelesaikan masalah dan membuat keputusan';
$lang['instruct_core_value_section_3_sub_content_17'] = 'Berani membuat keputusan dan berani menghadapi risiko';
$lang['instruct_core_value_section_3_sub_title_2']   = 'Berusaha jauhi';
$lang['instruct_core_value_section_3_sub_content_21'] = 'Selesa dengan penyelesaian masalah dengan cara lama';
$lang['instruct_core_value_section_3_sub_content_22'] = 'Tidak berusaha menjadi pencetus idea';
$lang['instruct_core_value_section_3_sub_content_23'] = 'Malas mendapatkan maklumat untuk menyelesaikan masalah yang sukar';
$lang['instruct_core_value_section_3_sub_content_24'] = 'Membuat keputusan sebelum berbincang';
$lang['instruct_core_value_section_3_sub_content_25'] = 'Sambil lewa menyelesaikan masalah';
$lang['instruct_core_value_section_3_sub_content_26'] = 'Tidak membantu rakan-rakan menyelesaikan masalah';
$lang['instruct_core_value_section_3_sub_content_27'] = 'Takut membuat keputusan dan menghadapi risiko';
// 4
$lang['instruct_core_value_section_4_title']         = 'Cekal dalam menghadapi cabaran';
$lang['instruct_core_value_section_4_sub_title_1']   = 'Jadikan amalan';
$lang['instruct_core_value_section_4_sub_content_11'] = 'Tidak mudah putus asa dan bertukar-tukar kerja';
$lang['instruct_core_value_section_4_sub_content_12'] = 'Berani mengambil risiko';
$lang['instruct_core_value_section_4_sub_content_13'] = 'Menyiapkan tugas yang penting walaupun terpaksa pulang lewat';
$lang['instruct_core_value_section_4_sub_content_14'] = 'Berusaha keras menyempurnakan tugas sehingga selesai';
$lang['instruct_core_value_section_4_sub_content_15'] = 'Mengenepikan kepentingan diri untuk kepentingan organisasi';
$lang['instruct_core_value_section_4_sub_content_16'] = 'Cekal dan kuat dalam menghadapai halangan dan cabaran';
$lang['instruct_core_value_section_4_sub_content_17'] = 'Sentiasa tahu menggunakan sumber yang ada dan mengetahui kemampuan diri';
$lang['instruct_core_value_section_4_sub_title_2']   = 'Berusaha jauhi';
$lang['instruct_core_value_section_4_sub_content_21'] = 'Putus asa dan tidak tetap hati';
$lang['instruct_core_value_section_4_sub_content_22'] = 'Takut mengambil risiko';
$lang['instruct_core_value_section_4_sub_content_23'] = 'Meninggalkan tugas yang penting dan mengutamakan kepentingan peribadi';
$lang['instruct_core_value_section_4_sub_content_24'] = 'Membiarkan tugas terbengkalai';
$lang['instruct_core_value_section_4_sub_content_25'] = 'Mementingkan kepentingan diri lebih dari kepentingan organisasi';
$lang['instruct_core_value_section_4_sub_content_26'] = 'Lemah jika menghadapi halangan dan cabaran';
$lang['instruct_core_value_section_4_sub_content_27'] = 'Tidak tahu menggunakan sumber yang ada serta tidak tahu kemampuan diri';
// 5
$lang['instruct_core_value_section_5_title']         = 'Proaktif dalam tindakan';
$lang['instruct_core_value_section_5_sub_title_1']   = 'Jadikan amalan';
$lang['instruct_core_value_section_5_sub_content_11'] = 'Mengambil inisiatif untuk membina kerjaya yang lebih baik';
$lang['instruct_core_value_section_5_sub_content_12'] = 'Memastikan hasil tugasan adalah yang terbaik';
$lang['instruct_core_value_section_5_sub_content_13'] = 'Merasakan sumbangan kepada organisasi tidak banyak';
$lang['instruct_core_value_section_5_sub_content_14'] = 'Yakin bahawa tiada masalah yang terlalu besar sehingga tidak dapat diselesaikan';
$lang['instruct_core_value_section_5_sub_content_15'] = 'Banyak berfikir dan merancang untuk masa depan';
$lang['instruct_core_value_section_5_sub_content_16'] = 'Berusaha mencapai lebih dari yang ditetapkan dalam KPI';
$lang['instruct_core_value_section_5_sub_content_17'] = 'Mengambil kira kesan dari setiap aspek apabila membuat keputusan';
$lang['instruct_core_value_section_5_sub_title_2']   = 'Berusaha jauhi';
$lang['instruct_core_value_section_5_sub_content_21'] = 'Tiada inisiatif untuk membina kerjaya';
$lang['instruct_core_value_section_5_sub_content_22'] = 'Hasil tugasan ala kadar dan melepas batuk di tangga';
$lang['instruct_core_value_section_5_sub_content_23'] = 'Merasa telah memberi sumbangan yang banyak kepada organisasi';
$lang['instruct_core_value_section_5_sub_content_24'] = 'Menganggap banyak masalah tidak dapat diselesaikan';
$lang['instruct_core_value_section_5_sub_content_25'] = 'Tidak berfikir dan merancang untuk masa depan';
$lang['instruct_core_value_section_5_sub_content_26'] = 'Tidak bersungguh mencapai KPI yang ditetapkan';
$lang['instruct_core_value_section_5_sub_content_27'] = 'Tidak mengambil kira kesan dari setiap aspek apabila membuat keputusan';

// dalil
$lang['dalil_core_1_text_1'] = '';
$lang['dalil_text'] = 'Dalil';
// 1
$lang['dalil_core_1_title_1'] = 'Hubungan yang kuat dengan Yang Maha Pencipta';
$lang['dalil_core_1_text_1'] = 'Dan berpegang&nbsp;teguhlah    kamu sekalian kepada tali Allah (agama Islam), dan janganlah kamu    bercerai-berai; dan kenanglah nikmat Allah kepada kamu ketika kamu    bermusuh-musuhan (semasa jahiliyah dahulu), lalu Allah menyatukan di antara    hati kamu (sehingga kamu bersatu-padu dengan nikmat Islam), maka menjadilah    kamu dengan nikmat Allah itu orang-orang Islam yang bersaudara. Dan kamu    dahulu telah berada di tepi jurang neraka (disebabkan kekufuran kamu semasa    jahiliyah), lalu Allah selamatkan kamu dari neraka itu (disebabkan nikmat    Islam juga). Demikianlah Allah menjelaskan kepada kamu ayat-ayat    keteranganNya, supaya kamu mendapat petunjuk hidayahNya.<br>(A-li\'Imraan:103)';
$lang['dalil_core_1_text_2'] = 'Kecuali orang-orang yang bertaubat    (dari perbuatan munafik itu) dan memperbaiki amalan mereka (yang salah), dan    mereka pula berpegang&nbsp;teguh kepada (agama)    Allah, serta mengerjakan agama mereka dengan ikhlas kerana Allah, maka mereka    yang demikian itu ditempatkan bersama-sama orang-orang yang beriman (di dalam    Syurga); dan Allah akan memberikan orang-orang yang beriman itu pahala yang    amat besar.<br>(An-Nisaa\' :146)';
$lang['dalil_core_1_text_3'] = 'Dan berpeganglah kamu semuanya    kepada tali (agama) allah dan janganlah kamu bercerai-berai, dan ingatlah    akan nikmat Allah kepadamu ketika kamu dahulu (masa jahiliah)    bermusuh-musuhan, maka Allah mempersatukan hatimu, lalu menjadilah kamu    kerana nikmat Allah orang-orang yang bersaudara, dan kamu telah berada    ditepi  jurang neraka, lalu Allah    menyelamatkan kamu darinya. Demikianlah Allah menerangkan ayat-ayatnya kepada    kamu agar kamu mendapat petunjuk&rdquo;(Ali Imran: 103)';
// 2
$lang['dalil_core_2_title_1'] = 'Teguh dalam mempertahankan prinsip yang disepakati';
$lang['dalil_core_2_text_1'] = 'Dan sesiapa yang berserah diri    bulat-bulat kepada Allah (dengan ikhlas) sedang ia berusaha mengerjakan    kebaikan, maka sesungguhnya ia telah berpegang&nbsp;kepada    simpulan (tali agama) yang teguh dan (ingatlah) kepada Allah jualah kesudahan    segala urusan.<br>(Luqman:22)';
$lang['dalil_core_2_text_2'] = '&ldquo;… dan tolong-menolonglah kamu dalam    (mengerjakan) kebajikan dan takwa, dan jangan tolong-menolong dalam berbuat    dosa dan pelanggaran. Dan bertakwalah kamu kepada Allah, sesungguhnya Allah    amat berat siksa-Nya.&rdquo; (Al-Maidah:2)';
$lang['dalil_core_2_text_3'] = '&quot;Sesungguhnya Allah sukakan orang-orang yang berjuang    di jalan-Nya dalam barisan seolah-olah satu binaan yang tersusun kukuh&quot;.(al-Saff:4)';
$lang['dalil_core_2_text_4'] = 'Maksud hadis Nabi Muhammad s.a.w<br>&quot;Hendaklah kamu berada dalam jamaah (kumpulan), kerana sesungguhnya berjamaah itu adalah rahmat, sedang perpecahan itu adalah azab&quot;. (riwayat Muslim)';
// 3
$lang['dalil_core_3_title_1'] = 'Kreatif    dalam membuat keputusan yang bijaksana';
$lang['dalil_core_3_text_1'] = '(Sesungguhnya    Allah juga) menyuruh kamu apabila menetapkan hukum sesama manusia, supaya    kamu menetapkan dengan penuh keadilan dan saksama&rdquo; (Al-Nisa&rsquo;: 58)';
$lang['dalil_core_3_text_2'] = 'Engkau    lebih mengetahui (mempunyai ilmu, kemahiran, kepakaran, kecekapan) dalam    urusan duniamu&rdquo; <br>(Riwayat    Anas dan Aishah)';
// 4
$lang['dalil_core_4_title_1'] = 'Cekal dalam menghadapi cabaran';
$lang['dalil_core_4_text_1'] = 'Dan Kami tidak mengutus Rasul-rasul    sebelummu (wahai Muhammad) melainkan orang-orang yang tentu makan minum dan    berjalan di pasar-pasar, dan Kami jadikan sebahagian dari kamu sebagai&nbsp;ujian&nbsp;dan cubaan bagi sebahagian    yang lain, supaya ternyata adakah kamu dapat bersabar (menghadapi&nbsp;ujian&nbsp;itu)? Dan (ingatlah) adalah    Tuhanmu sentiasa Melihat (akan keadaan makhluk-makhlukNya).<br>(Al-Furqaan :20';
$lang['dalil_core_4_text_2'] = 'Dan mintalah pertolongan (kepada    Allah) dengan jalan&nbsp;sabar&nbsp;dan    mengerjakan sembahyang; dan sesungguhnya sembahyang itu amatlah berat kecuali    kepada orang-orang yang khusyuk;<br>(Al-Baqarah 2:45)';
$lang['dalil_core_4_text_3'] = 'Wahai sekalian orang-orang yang    beriman! Mintalah pertolongan (untuk menghadapi susah payah dalam    menyempurnakan sesuatu perintah Tuhan) dengan bersabar&nbsp;dan    dengan (mengerjakan) sembahyang; kerana sesungguhnya Allah menyertai    (menolong) orang-orang yang&nbsp;sabar<br>(Al-Baqarah 2:153)';
$lang['dalil_core_4_text_4'] = '(Apa yang kamu katakan itu tidaklah    benar) bahkan sesiapa yang menyerahkan dirinya kepada Allah (mematuhi    perintahNya) sedang ia pula berusaha supaya baik&nbsp;amalannya, maka ia akan beroleh pahalanya di sisi    Tuhannya dan tidaklah ada kebimbangan (dari berlakunya kejadian yang tidak    baik) terhadap mereka, dan mereka pula tidak akan berdukacita.<br>(Al-Baqarah :112)';
// 5
$lang['dalil_core_5_title_1'] = 'Proaktif dalam tindakan';
$lang['dalil_core_5_text_1'] = 'Wahai Allah, tunjukilah kami jalan yang lurus.    Iaitu jalan orang-orang yang engkau telah berikan nikmat ke atas mereka dan    bukannya jalan orang-orang yang dimurkai oleh Engkau dan bukan pula jalan    orang-orang yang sesat.&rdquo;<br>(Al-Fatihah:    5-7)';
$lang['dalil_core_5_text_2'] = 'Dan orang-orang yang berpegang&nbsp;teguh dengan Kitab Allah    serta mendirikan sembahyang, sesungguhnya Kami tidak akan menghilangkan    pahala orang-orang yang berusaha memperbaiki (keadaan hidupnya).<br>(Al-A\'raaf:170)';
$lang['dalil_core_5_text_3'] = 'Sesungguhnya Allah beserta orang-orang    yang bertaqwa, dan orang-orang yang berusaha memperbaiki&nbsp;amalannya.<br>(Al-Nahl :128)';
$lang['dalil_core_5_text_4'] = 'Dan belanjakanlah (apa yang ada pada    kamu) kerana (menegakkan) agama Allah, dan janganlah kamu sengaja    mencampakkan diri kamu ke dalam bahaya kebinasaan (dengan bersikap bakhil);    dan baikilah (dengan sebaik-baiknya segala usaha dan) perbuatan kamu; kerana    sesungguhnya Allah mengasihi orang-orang yang berusaha memperbaiki&nbsp;amalannya.<br>(Al-Baqarah:195)';
$lang['dalil_core_5_text_5'] = 'Tidak ada dosa bagi orang-orang yang    beriman serta mengerjakan amal yang soleh pada apa yang telah mereka makan    dahulu, apabila mereka bertaqwa dan beriman serta mengerjakan amal yang    soleh, kemudian mereka tetap bertaqwa dan beriman, kemudian mereka tetap    bertaqwa dan berbuat kebajikan; kerana Allah mengasihi orang-orang yang    berusaha memperbaiki&nbsp;amalannya.<br>(Al-Maaidah :93)';
$lang['dalil_core_5_text_6'] = 'Dan janganlah kamu berbuat kerosakan    di bumi sesudah Allah menyediakan segala yang membawa kebaikan padanya, dan    berdoalah kepadaNya dengan perasaan bimbang (kalau-kalau tidak diterima) dan    juga dengan perasaan terlalu mengharapkan (supaya makbul). Sesungguhnya    rahmat Allah itu dekat kepada orang-orang yang memperbaiki&nbsp;amalannya.<br>
			          (Al-A\'raaf:56)';


// page-result
$lang['search_title']                = 'Carian Kriteria';
$lang['search_ic_text']              = 'No IC';
$lang['search_result_text']          = 'Keputusan';
$lang['search_faculty_text']         = 'Fakulti';
$lang['search_category_text']        = 'Kategori';
$lang['search_resultHigh_title']     = 'Keputusan anda dalam lingkungan 75-100.';
$lang['search_resutlMedium_title']   = 'Keputusan anda dalam lingkungan 50-74.';
$lang['search_resultLow_title']      = 'Keputusan anda dalam lingkungan 0-49.';
$lang['search_desc_title']           = 'Penerangan';
$lang['search_resultHigh_content']   = 'Tahniah! Index Uni-CoVa anda adalah cemerlang. Anda mempunyai ciri-ciri staf yang diperlukan oleh UMP. Semoga anda akan dapat terus memberi sumbangan bermakna kepada pembangunan UMP';
$lang['search_resultMedium_content'] = 'Index Uni-CoVa anda adalah sederhana. Selain memperbaiki kemahiran sedia ada,  anda perlu mempelajari dan meningkatkan kemahiran tertentu untuk menjadikan diri anda lebih cemerlang. Semoga  anda akan terus berjaya meningkatkan prestasi dan produktiviti';
$lang['search_resultLow_content']    = 'Index Uni-CoVa anda adalah rendah. Anda perlu berusaha gigih untuk memastikan anda dapat menghayati nilai teras universiti seterusnya mendapat kepuasan bekerja dan menyumbang kepada kemajuan universiti';
$lang['search_graph']                = 'Graf';
$lang['search_score']                = 'Markah';
$lang['search_cv1']                  = 'CV1';
$lang['search_cv2']                  = 'CV2';
$lang['search_cv3']                  = 'CV3';
$lang['search_cv4']                  = 'CV4';
$lang['search_cv5']                  = 'CV5';
$lang['search_total']                = 'Jumlah';
$lang['search_panduan_nilai']        = 'Panduan Nilai Teras';
$lang['search_dalil']                = 'Dalil';
$lang['search_'] = '';
$lang['search_'] = '';

// page questions
$lang['question_title']         = 'Soalan';
$lang['question_cat_title']     = 'Kategori';
$lang['question_ic_label']      = 'Sila masukkan No Kad Pengenalan';
$lang['question_faculty_label'] = 'Sila pilih fakulti';


// admin login
$lang['admin_login_heading']              = 'Log Masuk Admin';
$lang['admin_login_username_placeholder'] = 'Katanama';
$lang['admin_login_password_placeholder'] = 'Katalaluan';
$lang['admin_login_home'] = 'Utama';
$lang['admin_login_'] = '';

// admin list data
$lang['admin_listdata_heading'] = 'Senarai Data-data Nilai Teras';
$lang['admin_listdata_ic']      = 'No IC';
$lang['admin_listdata_faculty'] = 'Fakulti';
$lang['admin_listdata_cv1']     = 'CV1';
$lang['admin_listdata_cv2']     = 'CV2';
$lang['admin_listdata_cv3']     = 'CV3';
$lang['admin_listdata_cv4']     = 'CV4';
$lang['admin_listdata_cv5']     = 'CV5';
$lang['admin_listdata_total']   = 'Jumlah';
$lang['admin_listdata_']        = '';

// admin view result
$lang['admin_viewresult_heading'] = 'Carian Keputusan';
$lang['admin_viewresult_ic']      = 'No Kad Pengenalan';
$lang['admin_viewresult_matched_title']        = 'Rekod';
$lang['admin_viewresult_']        = '';

// admin data graph by faculry
$lang['admin_graph_heading'] = 'Maklumat Graf Mengikut Fakulti';
$lang['admin_graph_choose_faculty'] = 'Pilih Fakulti';
$lang['admin_graph_'] = '';
$lang['admin_graph_'] = '';
$lang['admin_graph_'] = '';
$lang['admin_graph_'] = '';
$lang['admin_graph_'] = '';
$lang['admin_graph_'] = '';

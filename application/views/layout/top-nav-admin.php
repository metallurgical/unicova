<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="nav">
            <?php
   			$siteLang = $this->session->userdata( 'site_lang' );?>
		    <select class="pull-right" id="changeLanguage" style="margin:10px 10px 0px;">
		    	<option value="malay" <?php if ( $siteLang == 'malay' ) echo 'selected'; ?>>Bahasa Melayu</option>
		    	<option value="english" <?php if ( $siteLang == 'english' ) echo 'selected'; ?>>Bahasa Inggeris</option>
		    </select>
            </div>
        </nav>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Unicova</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('vendor/twbs/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url('assets/css/ie10-viewport-bug-workaround.css'); ?>" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('assets/css/jumbotron-narrow.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/jquery.wizard/jquery.wizard.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    
    <script src="<?php echo base_url('assets/js/ie10-viewport-bug-workaround.js'); ?>"></script>
    <script src="<?php echo base_url('bower_components/jQuery/dist/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery.wizard/jquery.wizard.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/highcharts.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/highcharts-more.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/exporting.js'); ?>"></script>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .hideMe, #searchContainer, #resultContainer{
        display : none;
    }
    </style>
</head>
<body>
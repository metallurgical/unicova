<footer class="footer">
    <p>&copy; Copyright @ 2016 Uni-CoVa. All rights reserved. <a href="<?php echo base_url("admin"); ?>"><?php echo lang( 'footer_admin' ); ?></a>
    <?php
    $siteLang = $this->session->userdata( 'site_lang' );?>
    <select class="pull-right" id="changeLanguage">
    	<option value="malay" <?php if ( $siteLang == 'malay' ) echo 'selected'; ?>>Bahasa Melayu</option>
    	<option value="english" <?php if ( $siteLang == 'english' ) echo 'selected'; ?>>Bahasa Inggeris</option>
    </select>
    </p>
</footer>

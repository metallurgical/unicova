<div class="navbar nav_title" style="border: 0;">
    <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>Uni-Cova</span></a>
</div>
<div class="clearfix"></div>

<!-- menu prile quick info -->
<div class="profile">
    <div class="profile_pic">
        <img src="<?php echo base_url('assets/images/avatar_2x.png');?>" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span><?php echo lang( 'sitelogo_welcome' ); ?>,</span>
        <h2><?php echo lang( 'sitelogo_admin' ); ?></h2>
    </div>
</div>
<!-- /menu prile quick info -->
<div class="menu_section">
    <h3><?php echo lang( 'menu_admin_menus' ); ?></h3>
    <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> <?php echo lang( 'menu_admin_results' ); ?> <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="display: none">
                <li><a href="<?php echo base_url('admin/viewResult'); ?>"><?php echo lang( 'menu_admin_view_result' ); ?></a>
                </li>
                <li><a href="<?php echo base_url('admin/listData'); ?>"><?php echo lang( 'menu_admin_list_data' ); ?></a>
                </li>
                <li><a href="<?php echo base_url('admin/facultyresult'); ?>"><?php echo lang( 'menu_admin_by_faculty' ); ?></a>
                </li>
            </ul>
        </li>
        <!-- <li><a><i class="fa fa-edit"></i> Menu2 <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="display: none">
                <li><a href="empty.html">Menu2.1</a>
                </li>
                <li><a href="empty.html">Meny2.2s</a>
                </li>
            </ul>
        </li> -->
    </ul>
</div>
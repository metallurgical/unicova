                <!-- footer content -->

                <!-- <footer>
                    <div class="copyright-info">
                        <p class="pull-left">&copy; Copyright @ 2016 Uni-CoVa. All rights reserved.</a>      
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer> -->
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

  </div>  
  <script src="<?php echo base_url('assets/admin/js/bootstrap.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/gauge/gauge.min.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/progressbar/bootstrap-progressbar.min.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/nicescroll/jquery.nicescroll.min.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/icheck/icheck.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/moment/moment.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/datepicker/daterangepicker.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/chartjs/chart.min.js');?>"></script>

  <script src="<?php echo base_url('assets/admin/js/custom.js');?>"></script>
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.pie.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.orderBars.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.time.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/date.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.spline.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.stack.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/curvedLines.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/flot/jquery.flot.resize.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/maps/jquery-jvectormap-2.0.3.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/maps/gdp-data.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/maps/jquery-jvectormap-world-mill-en.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/admin/js/maps/jquery-jvectormap-us-aea-en.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/pace/pace.min.js');?>"></script> 
  <script src="<?php echo base_url('assets/admin/js/skycons/skycons.min.js');?>"></script>
  <!-- Datatables -->
  <script src="<?php echo base_url('assets/admin/js/datatables/js/jquery.dataTables.js');?>"></script>
  <script src="<?php echo base_url('assets/admin/js/datatables/tools/js/dataTables.tableTools.js');?>"></script>
  <script src="<?php echo base_url('assets/js/uni.js');?>"></script>
  <script type="text/javascript">
$(function () {
  $( '#changeLanguage' ).change( function () {
      var siteLang = $( this ).val();
      window.location.href = '<?php echo base_url( "multilanguage/switchlang/' + siteLang + '" ); ?>';
  });
})
  </script>
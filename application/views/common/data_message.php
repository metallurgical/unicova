<?php
                //if wanna add any more type, just add the array value here!!! 
$arr = array(                 
             array('text' => $this->session->flashdata('add'), 'alert_type' => 'alert-success'),
             array('text' => $this->session->flashdata('error'), 'alert_type' => 'alert-danger'),
             array('text' => $this->session->flashdata('delete'), 'alert_type' => 'alert-success'),
             array('text' => $this->session->flashdata('update'), 'alert_type' => 'alert-success'),
             array('text' => $this->session->flashdata('login_false'), 'alert_type' => 'alert-danger'),
             array('text' => $this->session->flashdata('user_exist'), 'alert_type' => 'alert-danger')
        );

foreach ( $arr as $key => $value ) {

    if( $value['text'] ) {
        ?>
        <div class="alert <?php echo $value['alert_type'];?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> 
            <b><?php echo $value['text'];?> </b> 
         </div>
     <?php
    }
    
}
?>
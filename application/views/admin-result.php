<?php $this->load->view('layout/header-admin' ); ?>
<style type="text/css">
.hideMe, #searchContainer, #resultContainer{
    display : none;
}
</style>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <?php $this->load->view('layout/sitelogo-admin' ); ?>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <?php $this->load->view('layout/menu-admin' ); ?>
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <?php $this->load->view('layout/menu-bottom-admin' ); ?>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<?php $this->load->view('layout/top-nav-admin' ); ?>
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo lang( 'admin_viewresult_heading' ); ?> <small><?php echo lang( 'search_title' ); ?></small></h2>                  
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang( 'admin_viewresult_ic' ); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="searchInput" maxlength="14" placeholder='881207-11-5567'>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" id="btnSearch"><?php echo lang( 'common_btn_search' ); ?></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="resultContainer">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo lang( 'admin_viewresult_matched_title' ); ?></h2>                  
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang( 'search_ic_text' ); ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="form-control-static" id="icOutput"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang( 'search_faculty_text' ); ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="form-control-static" id="facultyOutput"></p>
                            </div>
                        </div>                        
                        <hr/>
                        <div class="panel panel-default">  
                            <!-- Category -->
                            <div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  Uni-Cova Result</div>
                            <div class="panel-body">
                                <span><strong><?php echo lang( 'search_category_text' ); ?></strong></span><br/>
                                <p class="hideMe resultHigh"><?php echo lang( 'search_resultHigh_title' ); ?></p>
                                <p class="hideMe resultMedium"><?php echo lang( 'search_resutlMedium_title' ); ?></p>
                                <p class="hideMe resultLow"><?php echo lang( 'search_resultLow_title' ); ?></p>
                                <hr/>
                                <span><strong><?php echo lang( 'search_desc_title' ); ?></strong></span>
                                <p class="hideMe resultHigh">
                                    <?php echo lang( 'search_resultHigh_content' ); ?>
                                </p>
                                <p class="hideMe resultMedium">
                                    <?php echo lang( 'search_resultMedium_content' ); ?>
                                </p>
                                <p class="hideMe resultLow">
                                    <?php echo lang( 'search_resultLow_content' ); ?>
                                </p>
                                <hr/>               
                            </div>
                            <!-- Graph -->
                            <div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'search_graph' ); ?></div>
                            <div class="panel-body">
                                <div id="graffContainer"></div>             
                            </div>
                            <!-- Score -->
                            <div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'search_score' ); ?></div>
                            <div class="panel-body" id="scoreSubmit">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="badge badge-info"></span>
                                        <?php echo lang( 'search_cv1' ); ?>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-info"></span>
                                        <?php echo lang( 'search_cv2' ); ?>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-info"></span>
                                        <?php echo lang( 'search_cv3' ); ?>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-info"></span>
                                        <?php echo lang( 'search_cv4' ); ?>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-info"></span>
                                        <?php echo lang( 'search_cv5' ); ?>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary"></span>
                                        <?php echo lang( 'search_total' ); ?>
                                    </li>                   
                                </ul>           
                            </div>
                        </div>  
                  </form>
                </div>
            </div>
        </div>
    </div>
    <br />
<?php $this->load->view('layout/footer-admin' ); ?>
<script>
jQuery( function ( $ ) {   

    $( document ).on( 'click', '#btnSearch', function ( e ) {

        e.preventDefault();

        $.ajax({
            type : 'POST',
            url : '<?php echo base_url("user/fetchData");?>',
            data : {
                ic : $( '#searchInput' ).val()
            },
            dataType : 'json',
            beforeSend : function () {},
            success : function ( data ) {   

                var conf = getConfig(),
                    chart;

                if ( data.status == 2 ) {

                    alert( 'No data found' );
                    $( '#searchInput' ).val('');
                    $( '#searchInput' ).focus();
                    $( '#resultContainer' ).slideUp();
                    return;

                }

                var scoreTemp = parseInt( data.result.result_score_all ).toPrecision( 2 );

                if ( scoreTemp >= 0 && scoreTemp < 50 )
                    $( '.resultLow' ).css( 'display', 'block' );
                else if ( scoreTemp >= 50 && scoreTemp <= 74 )
                    $( '.resultMedium' ).css( 'display', 'block' );
                else
                    $( '.resultHigh' ).css( 'display', 'block' ); 

                $( '#resultContainer' ).slideDown();
                $( '#dataOutput' ).css( 'display','block' );
                $( '#facultyOutput' ).html( data.result.faculty );
                $( '#icOutput' ).html( data.result.ic );
                conf.series[0].data[0] = +data.result.result_score_category_1;
                conf.series[0].data[1] = +data.result.result_score_category_2;
                conf.series[0].data[2] = +data.result.result_score_category_3;
                conf.series[0].data[3] = +data.result.result_score_category_4;
                conf.series[0].data[4] = +data.result.result_score_category_5;
                var cat = $( '#scoreSubmit' ).css( 'display','block' ).find( 'span' );
                cat.eq( 0 ).text( +data.result.result_score_category_1 );
                cat.eq( 1 ).text( +data.result.result_score_category_2 );
                cat.eq( 2 ).text( +data.result.result_score_category_3 );
                cat.eq( 3 ).text( +data.result.result_score_category_4 );
                cat.eq( 4 ).text( +data.result.result_score_category_5 );
                cat.eq( 5 ).text( parseInt( data.result.result_score_all ).toPrecision( 2 ) );

                chart = new Highcharts.Chart( conf );
                    
                
            },
            complete : function () {}
        });
    });
    
});
</script>
</body>
</html>

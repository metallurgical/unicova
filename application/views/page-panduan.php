<div class="panel panel-default" id="panduan"><?php echo lang( '' ); ?>	
    <div class="panel-body">
    	<div class="panel panel-default">
    		<!-- search -->    		
			<div class="panel-heading"><i class="glyphicon glyphicon-tags"></i><?php echo lang( 'instruct_core_value_title' ); ?> <a href="#" class="btn btn-success btn-xs pull-right btnBack" data-target="page-result"><?php echo lang( 'common_btn_back' ); ?>	</a></div>
		    <div class="panel-body">
			    <p>
			    	<?php echo lang( 'instruct_core_value_content' ); ?>

						<ol>
							<li><?php echo lang( 'instruct_core_value_content_list_1' ); ?></li>
							<li><?php echo lang( 'instruct_core_value_content_list_2' ); ?></li>
							<li><?php echo lang( 'instruct_core_value_content_list_3' ); ?></li>
							<li><?php echo lang( 'instruct_core_value_content_list_4' ); ?></li>
							<li><?php echo lang( 'instruct_core_value_content_list_5' ); ?></li>
						</ol>
				</p>		    			    
		    </div>
    	</div>
    	<div class="panel panel-default">  
    		
			<div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'instruct_core_value_section_main_title_' ); ?>	</div>
		    <div class="panel-body">
		<!-- 1 -->
		    	<strong>1) <?php echo lang( 'instruct_core_value_section_1_title' ); ?>	</strong>
		    	<hr/>
		    	<ul>
		    		<li><?php echo lang( 'instruct_core_value_section_1_sub_title_1' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_11' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_12' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_13' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_14' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_15' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_16' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_17' ); ?>	</li>
		    			</ol>
		    		</li>
		    		<li><?php echo lang( 'instruct_core_value_section_1_sub_title_2' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_21' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_22' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_23' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_24' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_25' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_26' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_1_sub_content_27' ); ?>	</li>
		    			</ol>
		    		</li>
		    	</ul>
		<!-- 2 -->
		    	<strong>2) <?php echo lang( 'instruct_core_value_section_2_title' ); ?>	</strong>
		    	<hr/>
		    	<ul>
		    		<li><?php echo lang( 'instruct_core_value_section_2_sub_title_1' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_11' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_12' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_13' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_14' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_15' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_16' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_17' ); ?>	</li>
		    			</ol>
		    		</li>
		    		<li><?php echo lang( 'instruct_core_value_section_2_sub_title_2' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_21' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_22' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_23' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_24' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_25' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_26' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_2_sub_content_27' ); ?>	</li>
		    			</ol>
		    		</li>
		    	</ul>
		<!-- 3 -->
		    	<strong>3) <?php echo lang( 'instruct_core_value_section_3_title' ); ?>	</strong>
		    	<hr/>
		    	<ul>
		    		<li><?php echo lang( 'instruct_core_value_section_3_sub_title_1' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_11' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_12' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_13' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_14' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_15' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_16' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_17' ); ?>	</li>
		    			</ol>
		    		</li>
		    		<li><?php echo lang( 'instruct_core_value_section_3_sub_title_2' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_21' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_22' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_23' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_24' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_25' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_26' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_3_sub_content_27' ); ?>	</li>
		    			</ol>
		    		</li>
		    	</ul>
		<!-- 4 -->
		    	<strong>4) <?php echo lang( 'instruct_core_value_section_4_title' ); ?>	</strong>
		    	<hr/>
		    	<ul>
		    		<li><?php echo lang( 'instruct_core_value_section_4_sub_title_1' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_11' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_12' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_13' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_14' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_15' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_16' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_17' ); ?>	</li>
		    			</ol>
		    		</li>
		    		<li><?php echo lang( 'instruct_core_value_section_4_sub_title_2' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_21' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_22' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_23' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_24' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_25' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_26' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_4_sub_content_27' ); ?>	</li>
		    			</ol>
		    		</li>
		    	</ul>
		<!-- 4 -->
		    	<strong>5) <?php echo lang( 'instruct_core_value_section_5_title' ); ?>	</strong>
		    	<hr/>
		    	<ul>
		    		<li><?php echo lang( 'instruct_core_value_section_5_sub_title_1' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_11' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_12' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_13' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_14' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_15' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_16' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_17' ); ?>	</li>
		    			</ol>
		    		</li>
		    		<li><?php echo lang( 'instruct_core_value_section_5_sub_title_2' ); ?>	
		    			<ol>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_21' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_22' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_23' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_24' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_25' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_26' ); ?>	</li>
		    				<li><?php echo lang( 'instruct_core_value_section_5_sub_content_27' ); ?>	</li>
		    			</ol>
		    		</li>
		    	</ul>
		    </div>		    
		</div>		    
    </div>
</div>

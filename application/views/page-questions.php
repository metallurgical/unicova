<div class="panel panel-default"><?php echo lang( '' ); ?>
    <div class="panel-body">
        <div data-wizard-init>
            <ul class="steps">
	            <?php
	            $incCat = 1;
	            $catLength = count( $cat );

	            foreach ( $cat as $key => $value ) {
	            ?>
	                <li data-step="<?php echo $incCat; ?>"><?php echo lang( 'cat_section_title' ).' '.$incCat.'/'.$catLength; ?></li>                
	            <?php
	            	$incCat++;
		        }
		        ?>
		        <!-- <li data-step="6"><?php echo lang( 'cat_section_title' ).' 6/'.$catLength; ?></li> -->
            </ul>
            <div class="steps-content">
	            <?php

	            $quesAnsw = array(

	            	'1' => array(),
	            	'2' => array(),
	            	'3' => array(),
	            	'4' => array(),
	            	'5' => array(),	            	
	            	'6' => array(),
	            	'7' => array(),
	            	'8' => array(),
	            	'9' => array(),
	            	'10' => array()
	            );

	            foreach ( $collection as $key => $value ) {

	            	$cat_id = $value['category_id'];

	            	array_push( $quesAnsw[ $cat_id ], $value );

	            }

	            $incCatForQuestion = 1;

	            foreach ( $cat as $keyCat => $valCat ) {
	            	
	            ?>
	                <div data-step="<?php echo $incCatForQuestion; ?>" class="questionParent">
	                	<input type="hidden" name="total[]" class="total" value="0">
						<input type="hidden" name="score[]" class="score" value="0">
	                    <h4><?php echo lang( 'question_cat_title' ); ?> : <?php echo $valCat['category_description']; ?></h4>
	                    <hr/>
	                    <div class="btn-group" role="group" aria-label="...">
				            <button type="button" class="btn btn-success btn-xs">
				                <span class="glyphicon glyphicon-thumbs-down"></span> 
				                <?php echo lang( 'common_btn_disagree' ); ?>
				            </button>
				            <button type="button" class="btn btn-default btn-xs">1</button>
				            <button type="button" class="btn btn-default btn-xs">2</button>
				            <button type="button" class="btn btn-default btn-xs">3</button>
				            <button type="button" class="btn btn-default btn-xs">4</button>
				            <button type="button" class="btn btn-default btn-xs">5</button>
				            <button type="button" class="btn btn-default btn-xs">6</button>
				            <button type="button" class="btn btn-default btn-xs">7</button>
				            <button type="button" class="btn btn-success btn-xs">
				                <span class="glyphicon glyphicon-thumbs-up"></span> 
				                <?php echo lang( 'common_btn_agree' ); ?>
				            </button>
				        </div>
				        <hr/>
	                    <label class="label label-info"><?php echo lang( 'question_title' ); ?></label>
	                    <br/><br/>
	                    <?php
	                    $incAnsw = 1;
	                    foreach ( $quesAnsw[ $valCat['category_id'] ] as $key => $value ) {
	                    ?>
		                    <p>
		                        <?php echo $value['question_name']; ?>
		                        <div class="btn-group" data-toggle="buttons">
						            <label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>1" autocomplete="off" value="<?php echo $value['a1']; ?>" class="btnChooseAnsw"> 1
									 </label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>2" autocomplete="off" class="btnChooseAnsw" value="<?php echo $value['a2']; ?>"> 2
									</label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>3" autocomplete="off" value="<?php echo $value['a3']; ?>" class="btnChooseAnsw"> 3
									</label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>4" autocomplete="off" value="<?php echo $value['a4']; ?>" class="btnChooseAnsw"> 4
									</label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>5" autocomplete="off" value="<?php echo $value['a5']; ?>" class="btnChooseAnsw"> 5
									</label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>6" autocomplete="off" value="<?php echo $value['a6']; ?>" class="btnChooseAnsw"> 6
									</label>
									<label class="btn btn-success">
									    <input type="radio" name="options<?php echo $incAnsw; ?>" id="a<?php echo $valCat['category_id'].$incAnsw; ?>7" autocomplete="off" value="<?php echo $value['a7']; ?>" class="btnChooseAnsw"> 7
									</label>
						        </div>
		                    </p>
		                    <hr/>
		                    					    	
	                    <?php

	                    	$incAnsw++;
				        }

				        if ( $incCatForQuestion == 5 ) { 
				        ?> 
							<?php echo lang( 'question_ic_label' ); ?>:
							<input type="text" name="ic" id="ic" placeholder="891208-03-5656" maxlength="14" class="form-control"><br/>
							<?php echo lang( 'question_faculty_label' ); ?> :
							<select name="faculty" id="faculty" class="form-control">
								<option value="">Select Faculty</option>
								<?php
								foreach( $faculty as $key => $getFal ) {?>
									<option value="<?php echo $getFal['faculty_name']; ?>"><?php echo $getFal['faculty_name']; ?></option>
								<?php 
								} ?>
							</select>														
				    	<?php
				    	}
				    	?>
	                </div>

	            <?php
	            	$incCatForQuestion++;
		        }
		        ?>        
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layout/header-admin' ); ?>
<style type="text/css">
.hideMe, #searchContainer, #resultContainer1{
    display : none;
}
</style>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <?php $this->load->view('layout/sitelogo-admin' ); ?>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <?php $this->load->view('layout/menu-admin' ); ?>
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <?php $this->load->view('layout/menu-bottom-admin' ); ?>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<?php $this->load->view('layout/top-nav-admin' ); ?>
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo lang( 'admin_graph_heading' ); ?></h2>                  
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <select id="faculty" class="form-control" name="faculty">
                                <option value="">--<?php echo lang( 'admin_graph_choose_faculty' ); ?>--</option>
                                <?php
                                foreach ( $faculty as $key => $value ) {?>
                                    <option value="<?php echo $value['faculty_name']; ?>"><?php echo $value['faculty_name']; ?></option>
                                <?php 
                                } ?>
                            </select>  
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <input type="button" class="btn btn-primary" value="<?php echo lang( 'common_btn_search' ); ?>" id="btnSearch">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="resultContainer">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo lang( 'admin_viewresult_matched_title' ); ?></h2>                  
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div id="graffContainer"></div>
                </div>
            </div>
        </div>
    </div>
    
    <br />
<?php $this->load->view('layout/footer-admin' ); ?>
<script>
jQuery( function ( $ ) {
    $( '#btnSearch' ).click( function () {
        
        var val = $( '#faculty' ).val();

        $.ajax({
            type : 'POST',
            url : '<?php echo base_url("admin/fetchData"); ?>',
            data : {
                faculty : $( '#faculty' ).val()
            },
            dataType : 'json',
            beforeSend : function () {},
            success : function ( data ) {

                var conf = getConfig(),
                    chart;

                if ( data.msg == 1 ) {                    
                    
                    conf.title.text = '<?php echo lang( "admin_listdata_faculty" ); ?> : ' + $( '#faculty' ).val();
                    conf.tooltip.pointFormat = $(conf.tooltip.pointFormat).find('b').append('<span> %</span>').end().html();

                    conf.series[0].data[0] = +parseInt( data.cv1 ).toPrecision(2);
                    conf.series[0].data[1] = +parseInt( data.cv2 ).toPrecision(2);
                    conf.series[0].data[2] = +parseInt( data.cv3 ).toPrecision(2);
                    conf.series[0].data[3] = +parseInt( data.cv4 ).toPrecision(2);
                    conf.series[0].data[4] = +parseInt( data.cv5 ).toPrecision(2);
                    $( '#' + conf.chart.renderTo ).empty();                    
                    $( '#resultContainer' ).slideDown(function(){
                        chart = new Highcharts.Chart( conf );
                    });
                    
                }
                else {

                    if ( chart ) {
                        chart.destroy();
                    }

                    alert( '<?php echo lang( "common_alert_no_data_found" ); ?>' );
                    $( '#' + conf.chart.renderTo ).empty();
                    $( '#resultContainer' ).slideUp();
                }
                
               
                
            },
            complete : function () {
                //$.mobile.loading( 'hide' );
            }
        });
    })
});
</script>
</body>
</html>

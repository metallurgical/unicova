<div class="panel panel-default" id="dalil"><?php echo lang( '' ); ?>	
    <div class="panel-body">
    	<div class="panel panel-default">
    		<!-- search -->    		
			<div class="panel-heading"><i class="glyphicon glyphicon-tags"></i><?php echo lang( 'instruct_core_value_title' ); ?> <a href="#" class="btn btn-success btn-xs pull-right btnBack" data-target="page-result"><?php echo lang( 'common_btn_back' ); ?>	</a></div>
		    <div class="panel-body">
			    <div class="panel panel-default">
			    	<!-- 1 -->    		    				
					<div class="panel-heading"><?php echo lang( 'dalil_core_1_title_1' ); ?></div>
				    <div class="panel-body">
				    	<strong><?php echo lang( 'dalil_text' ); ?></strong>
				    	<hr/>
					    <p><?php echo lang( 'dalil_core_1_text_1' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_1_text_2' ); ?></p>	
					    <p><?php echo lang( 'dalil_core_1_text_3' ); ?></p>	
				    </div>

				    <!-- 2 -->    		    				
					<div class="panel-heading"><?php echo lang( 'dalil_core_2_title_1' ); ?></div>
				    <div class="panel-body">
				    	<strong><?php echo lang( 'dalil_text' ); ?></strong>
				    	<hr/>
					    <p><?php echo lang( 'dalil_core_2_text_1' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_2_text_2' ); ?></p>	
					    <p><?php echo lang( 'dalil_core_2_text_3' ); ?></p>	
					    <p><?php echo lang( 'dalil_core_2_text_4' ); ?></p>	
				    </div>

				    <!-- 3 -->    		    				
					<div class="panel-heading"><?php echo lang( 'dalil_core_3_title_1' ); ?></div>
				    <div class="panel-body">
				    	<strong><?php echo lang( 'dalil_text' ); ?></strong>
				    	<hr/>
					    <p><?php echo lang( 'dalil_core_3_text_1' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_3_text_2' ); ?></p>
				    </div>

				    <!-- 4 -->    		    				
					<div class="panel-heading"><?php echo lang( 'dalil_core_4_title_1' ); ?></div>
				    <div class="panel-body">
				    	<strong><?php echo lang( 'dalil_text' ); ?></strong>
				    	<hr/>
					    <p><?php echo lang( 'dalil_core_4_text_1' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_4_text_2' ); ?></p>
					    <p><?php echo lang( 'dalil_core_4_text_3' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_4_text_3' ); ?></p>
				    </div>

				    <!-- 5 -->    		    				
					<div class="panel-heading"><?php echo lang( 'dalil_core_5_title_1' ); ?></div>
				    <div class="panel-body">
				    	<strong><?php echo lang( 'dalil_text' ); ?></strong>
				    	<hr/>
					    <p><?php echo lang( 'dalil_core_5_text_1' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_5_text_2' ); ?></p>
					    <p><?php echo lang( 'dalil_core_5_text_3' ); ?></p>		    			    
					    <p><?php echo lang( 'dalil_core_5_text_4' ); ?></p>
					    <p><?php echo lang( 'dalil_core_5_text_5' ); ?></p>
					    <p><?php echo lang( 'dalil_core_5_text_6' ); ?></p>
				    </div>
		    	</div>		    			    
		    </div>
    	</div>    			    
    </div>
</div>

<div class="jumbotron">
    <h1><?php echo lang( 'jumbo_heading' ); ?></h1>
    <p class="lead">
        <?php echo lang( 'jumbo_desc' ); ?>
    </p>
    <p>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-success">
                <span class="glyphicon glyphicon-thumbs-down"></span> 
                <?php echo lang( 'common_btn_disagree' ); ?>
            </button>
            <button type="button" class="btn btn-default">1</button>
            <button type="button" class="btn btn-default">2</button>
            <button type="button" class="btn btn-default">3</button>
            <button type="button" class="btn btn-default">4</button>
            <button type="button" class="btn btn-default">5</button>
            <button type="button" class="btn btn-default">6</button>
            <button type="button" class="btn btn-default">7</button>
            <button type="button" class="btn btn-success">
                <span class="glyphicon glyphicon-thumbs-up"></span> 
                <?php echo lang( 'common_btn_agree' ); ?>
            </button>
        </div>
    </p>
    <p>
        <button type="button" class="btn btn-primary" id="btnClickToStart">
            <?php echo lang( 'common_btn_click_to_start' ); ?> 
            <span class="glyphicon glyphicon-circle-arrow-right"></span>
        </button>
    </p>
</div>
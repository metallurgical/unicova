<?php $this->load->view( 'layout/header' ); ?>
<div class="container">
    <div class="header clearfix">
        <?php $this->load->view( 'layout/topbar' ); ?>
    </div>
    <div id="loadingGif">
        <center><img src="<?php echo base_url( "assets/images/loading.gif" ); ?>"> Please wait.......</center>
    </div>
    <div id="main-container">
        
    </div>

    <?php $this->load->view( 'layout/footer' ); ?>
</div>
<script type="text/javascript">
( function ( jQuery ) {

    jQuery( function ( $ ) {

        var
            mainContainer   = $( '#main-container' ),
            btnClickToStart = $( '#btnClickToStart' ),
            loadingGif = $( '#loadingGif' ),
            cachePage;

        mainContainer.load( '<?php echo base_url( "user/loadFile/page-main" ); ?>', function () {
            loadingGif.hide();
        });
        //mainContainer.load( '<?php echo base_url( "user/loadFile/page-dalil" ); ?>' );       

        mainContainer.on( 'click', '#btnClickToStart', function (e) {
            mainContainer.empty();
            loadingGif.show();
            e.stopPropagation();            
            mainContainer.load( '<?php echo base_url( "user/loadFile/page-questions" ); ?>', function () {
                loadingGif.hide();
                var mySelector = document.querySelector('[data-wizard-init]');
                $( mySelector )[ 'wizardByGiro']({
                    onCompleted : function () {
                        var
                            totalElem = $( '.total' ),
                            scoreElem = $( '.score' );  

                        $.ajax({
                            type : 'POST',
                            url : '<?php echo base_url("user/saveData"); ?>',
                            data : {
                                ic : $( '#ic' ).val(),
                                faculty : $( '#faculty' ).val(),
                                result_total_category_1 : totalElem.eq( 0 ).val(),
                                result_total_category_2 : totalElem.eq( 1 ).val(),
                                result_total_category_3 : totalElem.eq( 2 ).val(),
                                result_total_category_4 : totalElem.eq( 3 ).val(),
                                result_total_category_5 : totalElem.eq( 4 ).val(),
                                result_total_all        : localStorage.getItem( 'totalAll' ),
                                result_score_category_1 : +parseInt( scoreElem.eq( 0 ).val() ).toPrecision(2),
                                result_score_category_2 : +parseInt( scoreElem.eq( 1 ).val() ).toPrecision(2),
                                result_score_category_3 : +parseInt( scoreElem.eq( 2 ).val() ).toPrecision(2),
                                result_score_category_4 : +parseInt( scoreElem.eq( 3 ).val() ).toPrecision(2),
                                result_score_category_5 : +parseInt( scoreElem.eq( 4 ).val() ).toPrecision(2),
                                result_score_all        : localStorage.getItem( 'totalScore' )
                            },
                            dataType : 'json',
                            beforeSend : function () {},
                            success : function ( data ) {
                                mainContainer.empty();
                                loadingGif.show();
                                mainContainer.load( '<?php echo base_url( "user/loadFile/page-result" ); ?>', function () {
                                    
                                    loadingGif.hide();

                                    $( '#resultContainer' ).slideDown();
                                    var scoreTemp = localStorage.getItem( 'totalScore' );

                                    if ( scoreTemp >= 0 && scoreTemp < 50 )
                                        $( '.resultLow' ).css( 'display', 'block' );
                                    else if ( scoreTemp >= 50 && scoreTemp <= 74 )
                                        $( '.resultMedium' ).css( 'display', 'block' );
                                    else
                                        $( '.resultHigh' ).css( 'display', 'block' );

                                    $( '#dataOutput' ).css( 'display','block' );
                                    $( '#facultyOutput' ).html( data.data.faculty );
                                    $( '#icOutput' ).html( data.data.ic );

                                    var conf = getConfig(),
                                        chart;
                                        
                                        conf.series[0].data[0] = +parseInt( scoreElem.eq( 0 ).val() ).toPrecision(2);
                                        conf.series[0].data[1] = +parseInt( scoreElem.eq( 1 ).val() ).toPrecision(2);
                                        conf.series[0].data[2] = +parseInt( scoreElem.eq( 2 ).val() ).toPrecision(2);
                                        conf.series[0].data[3] = +parseInt( scoreElem.eq( 3 ).val() ).toPrecision(2);
                                        conf.series[0].data[4] = +parseInt( scoreElem.eq( 4 ).val() ).toPrecision(2);
                                        var cat = $( '#scoreSubmit' ).css( 'display','block' ).find( 'span' );
                                        cat.eq( 0 ).text( +parseInt( scoreElem.eq( 0 ).val() ).toPrecision(2) );
                                        cat.eq( 1 ).text( +parseInt( scoreElem.eq( 1 ).val() ).toPrecision(2) );
                                        cat.eq( 2 ).text( +parseInt( scoreElem.eq( 2 ).val() ).toPrecision(2) );
                                        cat.eq( 3 ).text( +parseInt( scoreElem.eq( 3 ).val() ).toPrecision(2) );
                                        cat.eq( 4 ).text( +parseInt( scoreElem.eq( 4 ).val() ).toPrecision(2) );
                                        cat.eq( 5 ).text( parseInt( localStorage.getItem( 'totalScore' ) ).toPrecision( 2 ) );
                                        chart = new Highcharts.Chart( conf );
                                });
                                
                            },
                            complete : function () {
                                //$.mobile.loading( 'hide' );
                            }
                        });
                    }
                });

                $( e.delegateTarget ).on( 'click', '.btnChooseAnsw', function () {                   
                    
                    var
                    _this        = $( this ),
                    currentValue = _this.val(),
                    closetParent = _this.closest( '.questionParent' );

                    _this.closest( 'div.btn-group' ).find( 'label' ).removeClass( 'active' );
                    _this.closest( 'label' ).addClass('active');
                    // call function to calculate total
                    // for particular section/category
                    calcSectionTotal ( closetParent );
                });

                

                
            });// end jquery Load
        });

        $( document ).on( {

            keyup : function ( e ) {

                var 
                    $this = $( this ),
                    textVal = $this.val();

                if ( !checkingstring( textVal ) ) {

                    alert( 'Number only' );
                    $this.val( textVal.substring( 0, textVal.length - 1 ) );
                    return;

                }

                if ( textVal.length == 7 ) {

                    if ( textVal.indexOf( '-' ) === -1 ) {

                        var splitedStr = textVal.split('');
                        splitedStr.splice( 6, 0, '-' );
                        $this.val( splitedStr.join('') );
                    }
                }
                else if ( textVal.length == 10 ) {

                    if ( textVal.indexOf( '-' ) !== -1 ) {

                        if ( e.which !== 8 ) {
                            var splitedStr = textVal.split('');
                            splitedStr.splice( 9, 0, '-' );
                            $this.val( splitedStr.join('') );
                        }
                    }
                }
                
                if ( /[0-9]{6}/ig.test( textVal ) && ( textVal.length == 6 && textVal.length < 7 ) ) {

                    if ( e.which !== 8 ) $this.val( textVal + '-' );

                }
                else if ( /[0-9]{6}\-[0-9]{2}/ig.test( textVal ) && textVal.length == 9 ) {

                    if ( e.which !== 8 ) $this.val( textVal + '-' );

                }
                

                
            },
            paste : function ( e ) {

                setTimeout( function () {

                    var 
                        $this = $( e.target ),
                        textVal = $this.val().toString(),
                        length = textVal.length,
                        splitedStr;

                    if ( /[0-9]{6}\-[0-9]{2}\-[0-9]{4}/ig.test( textVal ) ) return;

                    splitedStr = textVal.split('');

                    if ( length >= 9 ) {

                        splitedStr.splice( 6, 0, '-' );
                        splitedStr.splice( 9, 0, '-' );
                        $this.val( splitedStr.join('') );
                    }

                }, 0 );


            },
            blur : function ( e ) {

                var 
                    $this = $( e.target ),
                    textVal = $this.val().toString(),
                    length = textVal.length,
                    splitedStr;

                if ( /[0-9]{6}\-[0-9]{2}\-[0-9]{4}/ig.test( textVal ) ) return;

                splitedStr = textVal.split('');

                if ( length >= 9 ) {

                    splitedStr.splice( 6, 0, '-' );
                    splitedStr.splice( 9, 0, '-' );
                    $this.val( splitedStr.join('') );
                }

            }
        }, '#ic, #searchInput' );

        $( document ).on( 'click', '#btnSearch', function ( e ) {

            e.preventDefault();

            $.ajax({
                type : 'POST',
                url : '<?php echo base_url("user/fetchData");?>',
                data : {
                    ic : $( '#searchInput' ).val()
                },
                dataType : 'json',
                beforeSend : function () {},
                success : function ( data ) {   

                    var conf = getConfig(),
                        chart;

                    if ( data.status == 2 ) {

                        alert( 'No data found' );
                        $( '#searchInput' ).val('');
                        $( '#searchInput' ).focus();
                        $( '#resultContainer' ).slideUp();
                        return;

                    }

                    var scoreTemp = parseInt( data.result.result_score_all ).toPrecision( 2 );

                    if ( scoreTemp >= 0 && scoreTemp < 50 )
                        $( '.resultLow' ).css( 'display', 'block' );
                    else if ( scoreTemp >= 50 && scoreTemp <= 74 )
                        $( '.resultMedium' ).css( 'display', 'block' );
                    else
                        $( '.resultHigh' ).css( 'display', 'block' ); 

                    $( '#resultContainer' ).slideDown();
                    $( '#dataOutput' ).css( 'display','block' );
                    $( '#facultyOutput' ).html( data.result.faculty );
                    $( '#icOutput' ).html( data.result.ic );

                    //conf.chart.renderTo = 'graffContainerSearch';
                    conf.series[0].data[0] = +data.result.result_score_category_1;
                    conf.series[0].data[1] = +data.result.result_score_category_2;
                    conf.series[0].data[2] = +data.result.result_score_category_3;
                    conf.series[0].data[3] = +data.result.result_score_category_4;
                    conf.series[0].data[4] = +data.result.result_score_category_5;
                    var cat = $( '#scoreSubmit' ).css( 'display','block' ).find( 'span' );
                    cat.eq( 0 ).text( +data.result.result_score_category_1 );
                    cat.eq( 1 ).text( +data.result.result_score_category_2 );
                    cat.eq( 2 ).text( +data.result.result_score_category_3 );
                    cat.eq( 3 ).text( +data.result.result_score_category_4 );
                    cat.eq( 4 ).text( +data.result.result_score_category_5 );
                    cat.eq( 5 ).text( parseInt( data.result.result_score_all ).toPrecision( 2 ) );

                    chart = new Highcharts.Chart( conf );
                        
                    
                },
                complete : function () {}
            });
        });
        

        $( document ).on('click', 'a.loadFile', function ( e ) {
            mainContainer.empty();
            loadingGif.show();
            e.preventDefault();

            var 
                target = $( this ).data( 'target' ),
                url = '<?php echo base_url("user/loadFile/' + target +'"); ?>';

            if ( /^admin/ig.test( target ) ) {
                url = '<?php echo base_url("admin/loadFile/' + target +'"); ?>';
            }

            if ( target == 'page-panduan' || target == 'page-dalil' ) {
                cachePage = $( '#result' ).detach();
            }

            mainContainer.load( url, function () {
                loadingGif.hide();
                if ( target == 'page-result' ) {
                    $( '#searchContainer' ).slideDown();
                }
            });
        });

        $( document ).on('click', '.btnBack', function ( e ) {

            e.preventDefault();
            mainContainer.html( cachePage );

        });

        function calcSectionTotal ( closetParent ) {

            var 
                total = 0,
                score = 0;

            closetParent.find(':radio:checked').each( function ( i, e ) {
                total += parseInt( e.value );
            });

            closetParent.find( '.total' ).val( total );
            score = ( total / 49 ) * 100;
            closetParent.find( '.score' ).val( score );
            calcTotalAll();
            
        }

        function calcTotalAll () {

            var 
                totalAll = 0,
                totalScore = 0;

            $( '.total' ).each( function ( i, e ) {
                totalAll += parseInt( e.value )
            });

            localStorage.setItem( 'totalAll', totalAll );
            totalScore = ( localStorage.getItem( 'totalAll' ) / 245 ) * 100;
            localStorage.setItem( 'totalScore', totalScore );

        }



        function checkingLenght ( input ) {

            return input.length;
        }

        function checkingstring ( input ) {

            var flag = false;

            if ( /^[\d\s\-\b]+$/ig.test( input ) === true ) {
                flag = true;
            }

            //console.log(flag)

            return flag;
        }

        function getConfig () {

            var conf = {

                    chart: {                
                        polar: true,
                        type: 'area',
                        renderTo : 'graffContainer'
                    },

                    title: {
                        text: 'Result Uni-Cova',
                        x: -80
                    },

                    pane: {
                        size: '80%'
                    },

                    xAxis: {
                        categories: ['CV1', 'CV2', 'CV3', 'CV4', 'CV5'],
                        tickmarkPlacement: 'on',
                        lineWidth: 0
                    },

                    yAxis: {
                        gridLineInterpolation: 'polygon',
                        lineWidth: 0,
                        min: 0
                    },

                    tooltip: {
                        shared: true,
                        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
                    },

                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        y: 70,
                        layout: 'vertical'
                    },

                    series: [{
                        name: 'Score',
                        data: [],
                        pointPlacement: 'on'
                    }]

            }

            return conf;
        }
        

        $( '#changeLanguage' ).change( function () {
            var siteLang = $( this ).val();
            window.location.href = '<?php echo base_url( "multilanguage/switchlang/' + siteLang + '" ); ?>';
        });

    });

})( jQuery );
</script>
</body>
</html>

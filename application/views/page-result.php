<div class="panel panel-default" id="result"><?php echo lang( '' ); ?>
    <div class="panel-body">
    	<div class="panel panel-default" id="searchContainer">
    		<!-- search -->    		
			<div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'search_title' ); ?></div>
		    <div class="panel-body">
			    <form class="form-inline">
				  	<div class="form-group">
				    	<label class="sr-only" for="exampleInputAmount"><?php echo lang( 'search_ic_text' ); ?></label>
					    <div class="input-group">
					      	<div class="input-group-addon"><i class="glyphicon glyphicon-user"></i> <?php echo lang( 'search_ic_text' ); ?></div>
					      	<input type="text" class="form-control" id="searchInput" placeholder="Enter your IC Number" maxlength="14">					      	
					    </div>
					</div>
					<button type="submit" class="btn btn-primary" id="btnSearch"><?php echo lang( 'common_btn_search' ); ?></button>
				</form>			    			    
		    </div>
    	</div>
    	<div class="panel panel-default" id="resultContainer">  
    		<!-- Category -->
			<div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'search_result_text' ); ?></div>
		    <div class="panel-body">
		    	<br/>
				<a href="#" class="btn btn-warning loadFile" data-target="page-panduan"> <?php echo lang( 'search_panduan_nilai' ); ?></a>
				<a href="#" class="btn btn-warning loadFile" data-target="page-dalil"> <?php echo lang( 'search_dalil' ); ?> </a>
				<br/>
				<hr/>
		    	<span id="dataOutput" style="display:none">
					<?php echo lang( 'search_ic_text' ); ?> : <span id="icOutput"></span><br/>
					<?php echo lang( 'search_faculty_text' ); ?> : <span id="facultyOutput"></span>
				</span>
				<hr/>
			    <span><strong><?php echo lang( 'search_category_text' ); ?></strong></span><br/>
			    <p class="hideMe resultHigh"><?php echo lang( 'search_resultHigh_title' ); ?></p>
			    <p class="hideMe resultMedium"><?php echo lang( 'search_resutlMedium_title' ); ?></p>
			    <p class="hideMe resultLow"><?php echo lang( 'search_resultLow_title' ); ?></p>
			    <hr/>
			    <span><strong><?php echo lang( 'search_desc_title' ); ?></strong></span>
			    <p class="hideMe resultHigh">
			    	<?php echo lang( 'search_resultHigh_content' ); ?>
			    </p>
			    <p class="hideMe resultMedium">
			    	<?php echo lang( 'search_resultMedium_content' ); ?>
			    </p>
			    <p class="hideMe resultLow">
			    	<?php echo lang( 'search_resultLow_content' ); ?>
			    </p>
			    <hr/>			    
		    </div>
		    <!-- Graph -->
		    <div class="panel-heading"><i class="glyphicon glyphicon-tags"></i>  <?php echo lang( 'search_graph' ); ?></div>
		    <div class="panel-body">
			    <div id="graffContainer"></div>			    
		    </div>
		    <!-- Score -->
		    <div class="panel-heading"><i class="glyphicon glyphicon-tags"></i> <?php echo lang( 'search_score' ); ?></div>
		    <div class="panel-body" id="scoreSubmit">
			    <ul class="list-group">
					<li class="list-group-item">
					    <span class="badge badge-info"></span>
					    <?php echo lang( 'search_cv1' ); ?>
					</li>
					<li class="list-group-item">
					    <span class="badge badge-info"></span>
					    <?php echo lang( 'search_cv2' ); ?>
					</li>
					<li class="list-group-item">
					    <span class="badge badge-info"></span>
					    <?php echo lang( 'search_cv3' ); ?>
					</li>
					<li class="list-group-item">
					    <span class="badge badge-info"></span>
					   <?php echo lang( 'search_cv4' ); ?> 
					</li>
					<li class="list-group-item">
					    <span class="badge badge-info"></span>
					    <?php echo lang( 'search_cv5' ); ?>
					</li>
					<li class="list-group-item">
					    <span class="badge badge-primary"></span>
					    <?php echo lang( 'search_total' ); ?>
					</li>					
				</ul>		    
		    </div>
		</div>		    
    </div>
</div>

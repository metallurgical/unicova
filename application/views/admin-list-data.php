<?php $this->load->view('layout/header-admin' ); ?>
<style type="text/css">
.hideMe, #searchContainer, #resultContainer{
    display : none;
}
</style>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <?php $this->load->view('layout/sitelogo-admin' ); ?>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <?php $this->load->view('layout/menu-admin' ); ?>
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <?php $this->load->view('layout/menu-bottom-admin' ); ?>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<?php $this->load->view('layout/top-nav-admin' ); ?>
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo lang( 'admin_listdata_heading' ); ?></h2>                  
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <!-- <th>
                                  <input type="checkbox" class="tableflat">
                                </th> -->
                                <th><?php echo lang( 'admin_listdata_ic' ); ?> </th>
                                <th><?php echo lang( 'admin_listdata_faculty' ); ?> </th>
                                <th><?php echo lang( 'admin_listdata_cv1' ); ?></th>
                                <th><?php echo lang( 'admin_listdata_cv2' ); ?></th>
                                <th><?php echo lang( 'admin_listdata_cv3' ); ?> </th>
                                <th><?php echo lang( 'admin_listdata_cv4' ); ?> </th>
                                <th><?php echo lang( 'admin_listdata_cv5' ); ?> </th>
                                <th><?php echo lang( 'admin_listdata_total' ); ?> </th>
                                <!-- <th class=" no-link last"><span class="nobr">Action</span> -->
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        if ( !empty( $results ) ) {
                            foreach ( $results as $key => $value ) {?>
                            <tr class="even pointer">
                                <!-- <td class="a-center ">
                                  <input type="checkbox" class="tableflat">
                                </td> -->
                                <th><?php echo $value['ic']; ?> </th>
                                <th><?php echo $value['faculty']; ?> </th>
                                <th><?php echo $value['result_score_category_1']; ?></th>
                                <th><?php echo $value['result_score_category_2']; ?></th>
                                <th><?php echo $value['result_score_category_3']; ?> </th>
                                <th><?php echo $value['result_score_category_4']; ?> </th>
                                <th><?php echo $value['result_score_category_5']; ?> </th>
                                <th><?php echo number_format((float)$value['result_score_all'], 2, '.', ''); ?> </th>
                                <!-- <td class=" last"><a href="#">View</a> -->
                                </td>
                            </tr>
                        <?php 
                            }
                        }
                        ?>
                        </tbody>
                      </table>                    
                </div>
            </div>
        </div>
    </div>
    
    <br />
<?php $this->load->view('layout/footer-admin' ); ?>
<script>
var asInitVals = new Array();
jQuery( function ( $ ) {   

    $('input.tableflat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    
    var oTable = $('#example').dataTable({
        "oLanguage": {
          "sSearch": "Search all columns:"
        },
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [0]
          } //disables sorting for column one
        ],
        'iDisplayLength': 12,
        "sPaginationType": "full_numbers",
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
          "sSwfPath": "<?php echo base_url('assets/admin/js/datatables/tools/swf/copy_csv_xls_pdf.swf');?>"
        }
    });

    $("tfoot input").keyup(function() {
        /* Filter on the column based on the index of this element's parent <th> */
        oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
    });
    $("tfoot input").each(function(i) {
        asInitVals[i] = this.value;
    });
    $("tfoot input").focus(function() {
        if (this.className == "search_init") {
          this.className = "";
          this.value = "";
        }
    });
    $("tfoot input").blur(function(i) {
        if (this.value == "") {
          this.className = "search_init";
          this.value = asInitVals[$("tfoot input").index(this)];
        }
    });
    
    
});
</script>
</body>
</html>

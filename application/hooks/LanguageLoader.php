<?php
class LanguageLoader
{
    function initialize() {

        $ci =& get_instance();
        $ci->load->helper('language');

        $site_lang = $ci->session->userdata('site_lang');

        if ( $site_lang ) $langFlag = $site_lang;
        else $langFlag = 'malay';

        $ci->lang->load( 'uni', $langFlag );
        $ci->lang->load( 'ion_auth', $langFlag );
        $ci->lang->load( 'auth', $langFlag );
        
        $array = array(
            'site_lang' => $langFlag
        );
        
        $ci->session->set_userdata( $array );
    }
}
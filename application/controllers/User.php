<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	
	public function index()	{	
		$this->load->view( 'index' );
	}

	public function loadFile () {

		$pagePath = $this->uri->segment( 3 );
		$data['defaultVar'] = '';

		if ( $pagePath == 'page-questions' ) {

			$lang = $this->session->userdata('site_lang');
			$table = 'categories';
			$where = array( 'lang' => $lang );
			$data['cat'] = $this->u->get_all_rows( $table, $where );

			$table = 'categories';
			$where = array( 'categories.lang' => $lang );
			$tableNameToJoin = array( 'questions', 'answers' );
			$tableRelation = array(
					'categories.category_id = questions.category_id',
					'questions.question_id = answers.question_id'
				);
			$data['collection'] = $this->u->get_all_rows( $table, $where, false, $tableNameToJoin, $tableRelation );
			//print_r($data['collection']);
			$table = 'faculty';
			$data['faculty'] = $this->u->get_all_rows( $table );


		}

		$this->load->view( $pagePath, $data );
	}

	public function saveData () {

		date_default_timezone_set( 'Asia/Kuala_Lumpur' );
		$formData = $this->input->post();

		$table = 'results';
		$where = array( 'ic' => $formData['ic'] );
		$results = $this->u->get_specified_row( $table, $where );

		if ( !empty( $results ) ) {
			$status = 1;
			$data = array();
		}
		else {
			$status = 2;

			$table = 'results';
			$arrayData = array(
					'ic'                      => $formData['ic'],
					'faculty'                 => $formData['faculty'],
					'result_total_category_1' => $formData['result_total_category_1'],
					'result_total_category_2' => $formData['result_total_category_2'],
					'result_total_category_3' => $formData['result_total_category_3'],
					'result_total_category_4' => $formData['result_total_category_4'],
					'result_total_category_5' => $formData['result_total_category_5'],
					'result_total_all'        => $formData['result_total_all'],
					'result_score_category_1' => $formData['result_score_category_1'],
					'result_score_category_2' => $formData['result_score_category_2'],
					'result_score_category_3' => $formData['result_score_category_3'],
					'result_score_category_4' => $formData['result_score_category_4'],
					'result_score_category_5' => $formData['result_score_category_5'],
					'result_score_all'        => $formData['result_score_all'],
					'result_date_created'     => date( 'Y-m-d H:i:s' )
				);
			$this->u->insert_new_data( $arrayData,$table );
			$data = array( 'ic' => $formData['ic'], 'faculty' => $formData['faculty'] );
		}

		echo json_encode( 
			array(
				'status' => $status,
				'data' => $data
			)
		);
	}

	public function fetchData () {

		$formData = $this->input->post();

		$table = 'results';
		$where = array( 'ic' => $formData['ic'] );
		$results = $this->u->get_specified_row( $table, $where );

		if ( !empty( $results ) ) {

			$data = $results;
			$status = 1;
		}
		else {
			$status = 2;
			$data = 'none';
		}

		echo json_encode( 
				array(
					'status' => $status,
					'result' => $data
				)
			);
	}
}

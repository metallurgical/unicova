<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multilanguage extends CI_Controller {

	function __construct() {
		parent::__construct();		
	}

	function switchlang() {
		
		$typeLanguage = ( $this->uri->segment( 3 ) ) ? $this->uri->segment( 3 ) : 'malay';
		$this->load->library( 'user_agent' );
		$this->session->set_userdata('site_lang', $typeLanguage);
        redirect( $this->agent->referrer() );
		
	}

}
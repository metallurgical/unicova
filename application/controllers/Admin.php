<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();		
	}

	public function index()	{		
		
		$this->load->view( 'admin-login' );

		if ( $this->input->post() ) {

			$formData = $this->input->post();
			$remember = false;
			$flag     = $this->ion_auth->login( $formData['inputUsername'], $formData['inputPassword'], $remember );

			if ( $flag ) redirect( 'admin/listData');
			else {

				$messageType = 'login_false';
				$this->u->display_message( $messageType );
			}
		}
	}

	public function loadFile () {

		if ( !$this->ion_auth->is_admin() ) {
			redirect( 'user' );
		}

		$pagePath           = $this->uri->segment( 3 );
		$data['defaultVar'] = '';
		$this->load->view( $pagePath, $data );
	}

	public function viewResult () {

		if ( !$this->ion_auth->is_admin() ) {
			redirect( 'user' );
		}

		$this->load->view('admin-result');
	}

	public function listData () {

		if ( !$this->ion_auth->is_admin() ) {
			redirect( 'user' );
		}

		$table           = 'results';
		$data['results'] = $this->u->get_all_rows( $table );
		$this->load->view('admin-list-data', $data );
	}

	public function facultyresult () {		

		$table           = 'faculty';
		$data['faculty'] = $this->u->get_all_rows( $table );
		$this->load->view('admin-faculty-result', $data );
	}

	public function fetchData () {

		if ( $this->input->post() ) {

			$formData = $this->input->post();

			$table           = 'results';
			$where = array( 'faculty' => $formData['faculty']);
			$results = $this->u->get_all_rows( $table, $where );

			if ( !empty( $results ) ) {
				// exist
				$msg = 1;

				$cv1Total = 0;
				$cv2Total = 0;
				$cv3Total = 0;
				$cv4Total = 0;
				$cv5Total = 0;

				foreach ( $results as $key => $value ) {
					
					$cv1Total += $value['result_score_category_1'];
					$cv2Total += $value['result_score_category_2'];
					$cv3Total += $value['result_score_category_3'];
					$cv4Total += $value['result_score_category_4'];
					$cv5Total += $value['result_score_category_5'];
				}

				$subTotal = $cv1Total + $cv2Total + $cv3Total + $cv4Total + $cv5Total;

				$cv1Percent = ( $cv1Total != 0 ) ? ( $cv1Total / $subTotal ) * 100  : 0;				
				$cv2Percent = ( $cv2Total != 0 ) ? ( $cv2Total / $subTotal ) * 100  : 0;
				$cv3Percent = ( $cv3Total != 0 ) ? ( $cv3Total / $subTotal ) * 100  : 0;
				$cv4Percent = ( $cv4Total != 0 ) ? ( $cv4Total / $subTotal ) * 100  : 0;
				$cv5Percent = ( $cv5Total != 0 ) ? ( $cv5Total / $subTotal ) * 100  : 0;

			}
			else {

				// not exist
				$msg = 2;
				$cv1Percent = 0;				
				$cv2Percent = 0;
				$cv3Percent = 0;
				$cv4Percent = 0;
				$cv5Percent = 0;
			}

			$output = array(
					'msg' => $msg,
					'cv1' => $cv1Percent,
					'cv2' => $cv2Percent,
					'cv3' => $cv3Percent,
					'cv4' => $cv4Percent,
					'cv5' => $cv5Percent
				);

			echo json_encode( $output );
		}
	}

	public function logout () {
		$this->ion_auth->logout();
		redirect( 'admin' );
	}
}
